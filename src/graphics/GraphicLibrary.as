package graphics 
{
	import configs.constants.StarlingConst;
	import configs.MainConfig;
	import flash.display.Bitmap;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	import misc.bitmap.BmpUtil;
	import starling.display.Image;
	import starling.textures.Texture;
	import starling.textures.TextureSmoothing;
	/**
	 * ...
	 * @author kk
	 */
	public class GraphicLibrary {
		
		static public const BG_TILE_SIZE:int = 100;
		
		public static const HERO_GRAPHIC:int = 1;
		
		public static const WEAPON1_GRAPHIC:int = 100;
		public static const WEAPON2_GRAPHIC:int = 101;
		public static const WEAPON3_GRAPHIC:int = 102;
		public static const WEAPON4_GRAPHIC:int = 103;
		public static const WEAPON5_GRAPHIC:int = 104;
		public static const WEAPON6_GRAPHIC:int = 105;
		
		public static const BULLET1_GRAPHIC:int = 200;
		public static const BULLET2_GRAPHIC:int = 201;
		public static const BULLET3_GRAPHIC:int = 202;
		static public const BULLET4_GRAPHIC:int = 204;
		static public const BULLET5_GRAPHIC:int = 205;
		static public const BULLET6_GRAPHIC:int = 206;
		static public const BULLET7_GRAPHIC:int = 207;
		
		public static const ENEMY1_GRAPHIC:int = 300;
		public static const ENEMY2_GRAPHIC:int = 301;
		public static const ENEMY3_GRAPHIC:int = 302;
		static public const ENEMY4_GRAPHIC:int = 304;
		
		static public const BG_GRAPHIC:int = 400;
		
		private var textures:Dictionary = new Dictionary();
		private var images:Dictionary = new Dictionary();
		
		public function getGraphic(graphicAssetId:int):Image {
			
			if (textures[graphicAssetId] == null) {
				textures[graphicAssetId] = generateGraphic(graphicAssetId);
			}
			
			var image1:Image = new Image(textures[graphicAssetId] as Texture);
			image1.smoothing = TextureSmoothing.TRILINEAR;
			image1.pivotX = image1.width  * 0.5;
			image1.pivotY = image1.height * 0.5;
			
			
			return image1;
		}
		
		private function generateGraphic(graphicAssetId:int):Texture  
		{
			var sprite:Sprite = new Sprite();
			var shape:Shape;
			
			if (graphicAssetId == GraphicLibrary.HERO_GRAPHIC) {
				//Hero
				sprite = new Sprite();
				
				sprite.graphics.beginFill(0x889977);
				sprite.graphics.drawCircle(0, 0, 15);
				sprite.graphics.endFill();
				
				sprite.graphics.beginFill(0x889977);
				sprite.graphics.drawRect( -20, -10, 40, 20);
				sprite.graphics.endFill();
				
				sprite.graphics.beginFill(0xFFFFFF);
				sprite.graphics.drawCircle(0, -7.5, 5);
				sprite.graphics.endFill();
				/*
				shape = new Shape();
				shape.graphics.beginFill(0xFFFFFF);
				shape.graphics.drawCircle(0, 0, 10);
				shape.graphics.endFill();
				shape.y = -15;
				
				sprite.addChild(shape);*/
			}
			
			if (graphicAssetId == GraphicLibrary.WEAPON1_GRAPHIC) {
				//Weapon1
				sprite = new Sprite();
				sprite.graphics.beginFill(0x781454);
				sprite.graphics.drawRect(-2.5, -7.5, 5, 15);
				sprite.graphics.endFill();
			}
			
			if (graphicAssetId == GraphicLibrary.WEAPON2_GRAPHIC) {
				//Weapon2
				sprite = new Sprite();
				sprite.graphics.beginFill(0x524233);
				sprite.graphics.drawRect(-4, -17.5, 8, 35);
				sprite.graphics.endFill();
				sprite.graphics.beginFill(0x52FF33);
				sprite.graphics.drawRect(-6, -15, 12, 20);
				sprite.graphics.endFill();
			}
			
			if (graphicAssetId == GraphicLibrary.WEAPON3_GRAPHIC) {
				//Weapon3
				sprite = new Sprite();
				sprite.graphics.beginFill(0x112288);
				sprite.graphics.drawRect(-10, -10, 20, 30);
				sprite.graphics.endFill();
				sprite.graphics.beginFill(0xFF2288);
				sprite.graphics.drawRect(-5, -5, 15, 10);
				sprite.graphics.endFill();
			}
			
			if (graphicAssetId == GraphicLibrary.WEAPON4_GRAPHIC) {
				//Weapon3
				sprite = new Sprite();
				sprite.graphics.beginFill(0x112288);
				sprite.graphics.drawRect(-10, -10, 20, 20);
				sprite.graphics.endFill();
				sprite.graphics.beginFill(0x992288);
				sprite.graphics.drawCircle(0, 0, 5);
				sprite.graphics.endFill();
			}
			
			if (graphicAssetId == GraphicLibrary.WEAPON5_GRAPHIC) {
				//Weapon3
				sprite = new Sprite();
				sprite.graphics.beginFill(0xCC66EE);
				sprite.graphics.drawRect(-4, -15, 8, 30);
				sprite.graphics.endFill();
				sprite.graphics.beginFill(0x669966);
				sprite.graphics.drawCircle(0, -2, 5);
				sprite.graphics.endFill();
			}
			
			if (graphicAssetId == GraphicLibrary.WEAPON6_GRAPHIC) {
				//Weapon3
				sprite = new Sprite();
				sprite.graphics.beginFill(0x993300);
				sprite.graphics.drawRect(-4, -30, 8, 45);
				sprite.graphics.endFill();
				sprite.graphics.beginFill(0x996633);
				sprite.graphics.drawRect(-4, -20, 8, 10);
				sprite.graphics.endFill();
			}
			
			if (graphicAssetId == GraphicLibrary.BULLET1_GRAPHIC) {
				//Bullet1
				sprite = new Sprite();
				sprite.graphics.beginFill(0x545248);
				sprite.graphics.drawRect(-4, -1.5, 8, 3);
				sprite.graphics.endFill();
			}
			
			if (graphicAssetId == GraphicLibrary.BULLET2_GRAPHIC) {
				//Bullet2
				sprite = new Sprite();
				sprite.graphics.beginFill(0x992288);
				sprite.graphics.drawCircle(0, 0, 5);
				sprite.graphics.endFill();
			}
			
			
			if (graphicAssetId == GraphicLibrary.BULLET3_GRAPHIC) {
				//Bullet3
				sprite = new Sprite();
				sprite.graphics.beginFill(0x025478);
				sprite.graphics.drawCircle(0, 0, 3);
				sprite.graphics.endFill();
			}
			
			if (graphicAssetId == GraphicLibrary.BULLET4_GRAPHIC) {
				//Bullet4
				sprite = new Sprite();
				sprite.graphics.beginFill(0xFF0000);
				sprite.graphics.drawRect(-2.5, -7.5, 5, 15);
				sprite.graphics.drawRect(-7.5, -2.5, 15, 5);
				sprite.graphics.endFill();
			}
			
			if (graphicAssetId == GraphicLibrary.BULLET5_GRAPHIC) {
				//Bullet5
				sprite = new Sprite();
				sprite.graphics.beginFill(0x000000);
				sprite.graphics.drawRect(-2.5, -3.5, 5, 7);
				sprite.graphics.drawRect(-3.5, -2.5, 7, 5);
				sprite.graphics.endFill();
			}
			
			if (graphicAssetId == GraphicLibrary.BULLET6_GRAPHIC) {
				//Bullet6
				sprite = new Sprite();
				sprite.graphics.beginFill(0x775566);
				sprite.graphics.drawRect(-1, -3.5, 7, 2);
				sprite.graphics.endFill();
			}			
			
			if (graphicAssetId == GraphicLibrary.BULLET7_GRAPHIC) {
				//Bullet6
				sprite = new Sprite();
				sprite.graphics.beginFill(0xFF2222);
				sprite.graphics.drawRect(-1.5, -3.5, 7, 3);
				sprite.graphics.endFill();
			}
			
			if (graphicAssetId == GraphicLibrary.ENEMY1_GRAPHIC) {
				//Enemy1
				sprite = new Sprite();
				sprite.graphics.beginFill(0x7755DD);
				sprite.graphics.drawCircle(0, 0, 12);
				sprite.graphics.endFill();
				
				sprite.graphics.beginFill(0xDD5577);
				sprite.graphics.drawCircle(0, -6, 3);
				sprite.graphics.endFill();
			}
			
			if (graphicAssetId == GraphicLibrary.ENEMY2_GRAPHIC) {
				//Enemy2
				sprite = new Sprite();
				sprite.graphics.beginFill(0xDD7755);
				sprite.graphics.drawCircle(0, 0, 30);
				sprite.graphics.endFill();
				
				sprite.graphics.beginFill(0x5577DD);
				sprite.graphics.drawCircle(-10, -10, 7.5);
				sprite.graphics.endFill();
				
				sprite.graphics.beginFill(0x5577DD);
				sprite.graphics.drawCircle(10, -10, 7.5);
				sprite.graphics.endFill();
				
				sprite.graphics.beginFill(0x5577DD);
				sprite.graphics.drawCircle(0, -10, 7.5);
				sprite.graphics.endFill();
			}
			
			if (graphicAssetId == GraphicLibrary.ENEMY3_GRAPHIC) {
				//Enemy3
				sprite = new Sprite();
				sprite.graphics.beginFill(0x5577DD);
				sprite.graphics.drawCircle(0, 0, 8);
				sprite.graphics.endFill();
				
				sprite.graphics.beginFill(0xDD7755);
				sprite.graphics.drawCircle(0, -3, 1.5);
				sprite.graphics.endFill();
			}
			
			if (graphicAssetId == GraphicLibrary.ENEMY4_GRAPHIC) {
				//Enemy4
				sprite = new Sprite();
				sprite.graphics.beginFill(0x000000);
				sprite.graphics.drawCircle(0, 0, 20);
				sprite.graphics.endFill();
				
				sprite.graphics.beginFill(0xBBBBBB);
				sprite.graphics.drawCircle(0, -7.5, 5);
				sprite.graphics.endFill();
			}
			
			
			
			
			
			if (graphicAssetId == GraphicLibrary.BG_GRAPHIC) {
				//bakground
				sprite = new Sprite();
				
				var col:int = 0;
				
				
				for (var ix:int = 0; ix < MainConfig.MAP_MAX_WIDTH; ix+=GraphicLibrary.BG_TILE_SIZE) {
					for (var iy:int = 0; iy < MainConfig.MAP_MAX_WIDTH; iy+=GraphicLibrary.BG_TILE_SIZE) {
						
						if (col % 2) {
							sprite.graphics.beginFill(0xDDDDDD);
						}else {
							sprite.graphics.beginFill(0xFFFFFF) ;
						}
						
						sprite.graphics.drawRect(ix, iy, GraphicLibrary.BG_TILE_SIZE, GraphicLibrary.BG_TILE_SIZE);
						sprite.graphics.endFill();
						
						col++;
					}
					col++;
				}
			}
			
			
			var bitmap:Bitmap = BmpUtil.toBitmap(sprite);
			var texture:Texture = Texture.fromBitmap(bitmap);
			return texture;
		}
		
		
		
	}

}