package logic.starling.vo {
	/**
	 * ...
	 * @author Der Korser
	 */
	public class ScreenSizeVo {
		
		public var screenWidth:int;
		public var screenHeight:int;
		public var isFullScreen:Boolean;
	}

}