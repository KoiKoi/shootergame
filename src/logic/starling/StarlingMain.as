package logic.starling {
	import configs.constants.StarlingConst;
	import flash.display.StageDisplayState;
	import flash.geom.Rectangle;
	import logic.starling.event.EventStarlingReady;
	import logic.starling.event.EventStarlingScreenRefresh;
	import org.osflash.signals.Signal;
	import starling.core.Starling;
	import logic.starling.vo.ScreenSizeVo;
	import starling.display.Sprite;
	import starling.display.Stage;
	import starling.events.Event;
	/**
	 * ...
	 * @author Der Korser
	 */
	public class StarlingMain {
		
		private var _signal:Signal;
		private var _screenView:ScreenSizeVo;
		private var _starling:Starling;
		private var _startupClass:Main;
		
		public function StarlingMain(rootView:Main) {
			_startupClass = rootView;
			_signal = new Signal();
		}
		
		public function init():void {
			_starling = new Starling(MainStarlingView, _startupClass.stage);
			_starling.showStats = true;
			_starling.addEventListener(Event.ROOT_CREATED, onContext3DCreate);
			_starling.antiAliasing = 8;
			_starling.start();
		}
		
		private function onContext3DCreate(e:Event):void {
			_screenView = new ScreenSizeVo();
			_starling.removeEventListener(Event.ADDED_TO_STAGE, onContext3DCreate);
			Starling.current.antiAliasing = 16;
			Starling.current.nativeStage.frameRate = StarlingConst.STARLING_FPS;
			_startupClass.stage.addEventListener(Event.RESIZE, stageResizeHandler, false, int.MAX_VALUE, true);
			_starling.showStats = true;
			
			stageResizeHandler();
			_signal.dispatch(new EventStarlingReady());
		}
		
		private function stageResizeHandler(event:*=null):void {
			_starling.stage.stageWidth = _startupClass.stage.stageWidth;
			_starling.stage.stageHeight = _startupClass.stage.stageHeight;
			
			var viewPort:Rectangle = _starling.viewPort;
			viewPort.width = _startupClass.stage.stageWidth;
			viewPort.height = _startupClass.stage.stageHeight;
			
			try {
				_starling.viewPort = viewPort;
			} catch (error:Error) { }
			
			
			_screenView.isFullScreen = ((_startupClass.stage.displayState == StageDisplayState.FULL_SCREEN) || (_startupClass.stage.displayState == StageDisplayState.FULL_SCREEN_INTERACTIVE));
			
			if (_screenView.isFullScreen) {
				_screenView.screenWidth = _startupClass.stage.fullScreenWidth;
				_screenView.screenHeight = _startupClass.stage.fullScreenHeight;
			}else {
				_screenView.screenWidth = _startupClass.stage.stageWidth;
				_screenView.screenHeight = _startupClass.stage.stageHeight;
			}
			_signal.dispatch(new EventStarlingScreenRefresh(_screenView));
		}
		
		public function get starling():Starling {
			return _starling;
		}
		
		public function get signal():Signal {
			return _signal;
		}
		
		public function get mainView():Stage {
			return _starling.stage;
		}
		
		
	}

}