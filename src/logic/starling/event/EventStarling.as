package logic.starling.event {
	/**
	 * ...
	 * @author Der Korser
	 */
	public class EventStarling  {
		
		public static const STARLING_READY			:String = 'STARLING_READY';
		public static const STARLING_REFRESH		:String = 'STARLING_REFRESH';
		
		protected var _eventType:String;
		
		
		public function EventStarling() {
			
		}
		
		public function get eventType():String {
			return _eventType;
		}
		
	}

}