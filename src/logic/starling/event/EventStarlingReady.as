package logic.starling.event {
	/**
	 * ...
	 * @author Der Korser
	 */
	public class EventStarlingReady extends EventStarling {
		
		public function EventStarlingReady() {
			_eventType = EventStarling.STARLING_READY;
		}
		
	}

}