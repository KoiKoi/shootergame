package logic.starling.event {
	import logic.starling.vo.ScreenSizeVo;
	/**
	 * ...
	 * @author Der Korser
	 */
	public class EventStarlingScreenRefresh extends EventStarling {
		
		private var _screenSizeVo:ScreenSizeVo;
		
		public function EventStarlingScreenRefresh(screenVo:ScreenSizeVo) {
			_eventType = EventStarling.STARLING_REFRESH;
			_screenSizeVo = screenVo;
		}
		
		public function get screenSizeVo():ScreenSizeVo {
			return _screenSizeVo;
		}
		
	}

}