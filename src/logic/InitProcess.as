package logic 
{
	import configs.MainConfig;
	import logic.input.GamePadInput;
	import logic.input.TouchInput;
	import logic.starling.event.EventStarling;
	import starling.display.Image;
	import starling.display.Sprite;
	import graphics.GraphicLibrary;
	import logic.input.KeyboardInput;
	import logic.input.MouseInput;
	import logic.starling.StarlingMain;
	import playfield.Playfield;
	
	/**
	 * ...
	 * @author kk
	 */
	public class InitProcess {
		
		private var _mainView:Main;
		private var _mainConfig:MainConfig;
		private var _playfield:Playfield;
		private var _keyBoardInput:KeyboardInput;
		private var _mouseInput:MouseInput;
		private var _touchInput:TouchInput;
		private var _gamePadInput:GamePadInput;
		private var _starling:StarlingMain;
		
		public function InitProcess(initMainView:Main) {
			_mainView = initMainView;
			initStarling();
		}
		
		private function initStarling():void {
			_starling = new StarlingMain(_mainView);
			_starling.signal.add(initGame);
			_starling.init();
		}
		
		private function initGame(event:EventStarling):void {
			if (event.eventType == EventStarling.STARLING_READY) {
				
				_starling.signal.remove(initGame);
				//Setup the Game-Config of Hero, Weapons, Bullets Enemies ...
				_mainConfig = new MainConfig();
				
				//Init input Keyboard and Mouse
				_keyBoardInput = new KeyboardInput(_mainView);
				_touchInput = new TouchInput();
				_mouseInput = new MouseInput(false);
				_gamePadInput = new GamePadInput(_mainView);
				
				//Init Playfield
				_playfield = new Playfield(_mainConfig, _keyBoardInput.signal, _mouseInput.signal, _touchInput.signal, _gamePadInput.signal);
				
				//SetMouseFocus on the Playfield-background
				_mouseInput.init(_mainView.stage);
				_touchInput.init(_playfield.background);
				
				
				//Add PlayField to the main Stage
				_starling.mainView.addChild(_playfield);
			}
		}
		
	}

}