package logic.input 
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import logic.input.state.MouseState;
	import org.osflash.signals.Signal;
	/**
	 * ...
	 * @author kk
	 */
	public class MouseInput {
		
		private var _signal:Signal;
		private var _mainView:DisplayObject;
		private var _mouseState:MouseState;
		private var _useCoordinates:Boolean;
		
		public function MouseInput(useCoord:Boolean = true, main:DisplayObject = null) {
			_mouseState = new MouseState();
			_signal = new Signal();
			_useCoordinates = useCoord;
			if (main) {
				init(main);
			}
		}
		
		
		public function init(main:DisplayObject):void {
			
			if (main) {
				_mainView = main;
				
				//Mouse Move
				if (_useCoordinates) {
					_mainView.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
					_mainView.addEventListener(FocusEvent.FOCUS_OUT, onFocusLost);
					_mainView.addEventListener(FocusEvent.MOUSE_FOCUS_CHANGE, onFocusLost);
					_mainView.addEventListener(FocusEvent.KEY_FOCUS_CHANGE, onFocusLost);
					_mainView.addEventListener(Event.DEACTIVATE, onDeactivate);
				}
				
				//Right Mouse
				_mainView.addEventListener(MouseEvent.RIGHT_MOUSE_DOWN, onMouseRightDown);
				_mainView.addEventListener(MouseEvent.RIGHT_MOUSE_UP, onMouseRightUp);
				
				
				//Left Mouse
				_mainView.addEventListener(MouseEvent.MOUSE_DOWN, onMouseLeftDown);
				_mainView.addEventListener(MouseEvent.MOUSE_UP, onMouseLeftUp);
				
			}
			
			
		}
		
		private function onFocusLost(e:FocusEvent):void {
			onDeactivate();
		}
		
		private function onDeactivate(e:Event=null):void {
			_mouseState.rightButtonPressed = false;
			_mouseState.leftButtonPressed = false;
			_signal.dispatch(_mouseState);
		}
		
		private function onMouseRightUp(e:MouseEvent):void {
			_mouseState.rightButtonPressed = false;
			_signal.dispatch(_mouseState);
		}
		
		private function onMouseRightDown(e:MouseEvent):void {
			_mouseState.rightButtonPressed = true;
			_signal.dispatch(_mouseState);
		}
		
		private function onMouseMove(e:MouseEvent):void {
			_mouseState.position.x = _mainView.mouseX;
			_mouseState.position.y = _mainView.mouseY;
			_signal.dispatch(_mouseState);
		}
		
		private function onMouseLeftDown(e:MouseEvent):void {
			_mouseState.leftButtonPressed = true;
			_signal.dispatch(_mouseState);
		}
		
		private function onMouseLeftUp(e:MouseEvent):void {
			_mouseState.leftButtonPressed = false;
			_signal.dispatch(_mouseState);
		}
		
		public function get signal():Signal {
			return _signal;
		}
		
	}
	
}