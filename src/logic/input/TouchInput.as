package logic.input 
{
	import flash.events.Event;
	import logic.input.state.TouchState;
	import org.osflash.signals.Signal;
	import starling.display.DisplayObject;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	/**
	 * ...
	 * @author kk
	 */
	public class TouchInput {
		
		private var _signal:Signal;
		private var _mainView:DisplayObject;
		private var _touchState:TouchState;
		
		public function TouchInput(main:DisplayObject = null) {
			
			_touchState = new TouchState();
			_signal = new Signal();
			
			if (main) {
				init(main);
			}
		}
		
		
		public function init(main:DisplayObject):void {
			
			if (main) {
				_mainView = main;
				_mainView.touchable = true;
				_mainView.addEventListener(TouchEvent.TOUCH, onTouch);
				
				//Left Mouse
				/*
				_mainView.addEventListener(MouseEvent.MOUSE_DOWN, onMouseLeftDown);
				_mainView.addEventListener(MouseEvent.MOUSE_UP, onMouseLeftUp);
				
				_mainView.addEventListener(FocusEvent.FOCUS_OUT, onFocusLost);
				_mainView.addEventListener(FocusEvent.MOUSE_FOCUS_CHANGE, onFocusLost);
				_mainView.addEventListener(FocusEvent.KEY_FOCUS_CHANGE, onFocusLost);
				_mainView.addEventListener(Event.DEACTIVATE, onDeactivate);*/
			}
			
			
		}
		
		private function onTouch(event:TouchEvent):void {
			_touchState.position.x = event.touches[0].globalX;
			_touchState.position.y = event.touches[0].globalY;
			_signal.dispatch(_touchState);
		}
		
		
		/*
		private function onFocusLost(e:Event):void {
			onDeactivate();
		}*/
		
		private function onDeactivate(e:Event=null):void {
			_touchState.rightButtonPressed = false;
			_touchState.leftButtonPressed = false;
			_signal.dispatch(_touchState);
		}
		
		
		public function get signal():Signal {
			return _signal;
		}
		
	}
	
}