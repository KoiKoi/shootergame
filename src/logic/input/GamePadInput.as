package logic.input {
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import logic.input.state.GamePadState;
	import misc.controller.constants.ControlButtonsConst;
	import misc.controller.constants.ControlEventConst;
	import misc.controller.controller.GameController;
	import misc.controller.ControllerInput;
	import misc.controller.event.ControllerButtonEvent;
	import misc.controller.event.ControllerDirectionalEvent;
	import misc.controller.event.ControllerJoystickEvent;
	import misc.controller.event.ControllerTriggerEvent;
	import misc.controller.event.GameControllerConnectEvent;
	import misc.controller.event.GameControllerMainEvent;
	import org.osflash.signals.Signal;
	
	/**
	 * ...
	 * @author kk
	 */
	public class GamePadInput {
		
		private var _signal:Signal;
		private var _mainView:DisplayObject;
		private var _gamePadState:GamePadState;
		
		private var _controllerInput:ControllerInput;
		private var _controllerOne:GameController;
		
		public function GamePadInput(main:DisplayObject = null) {
			
			_gamePadState = new GamePadState();
			_signal = new Signal();
			
			if (main) {
				init(main);
			}
		}
		
		
		public function init(main:DisplayObject):void {
			
			if (main) {
				_mainView = main;
				_controllerInput = new ControllerInput();
				_controllerInput.signal.add(onControllerInit);
				_controllerInput.initialize(main);
			}
			
		}
		
		private function onControllerInit(event:GameControllerMainEvent):void {
			if (event.type == ControlEventConst.ON_CONTROLLER_CONNECT) {
				var controllEvent:GameControllerConnectEvent = (event as GameControllerConnectEvent);
				if (_controllerOne == null) {
					_controllerOne = controllEvent.controller;
					_controllerOne.enable();
					_controllerOne.controllerSignal.add(onButtonPress);
				}
			}
		}
		
		private function onButtonPress(event:GameControllerMainEvent):void {
			
			if (event.type == ControlEventConst.ON_CONTROLLER_BUTTON) {
				var buttonEvent:ControllerButtonEvent = (event as ControllerButtonEvent);
				_gamePadState.lastInteraction = buttonEvent.controlInput.id;
				
				if (buttonEvent.controlInput.id == ControlButtonsConst.BUTTON_A) {
					_gamePadState.buttonA = buttonEvent.controlInput.held;
				}
				if (buttonEvent.controlInput.id == ControlButtonsConst.BUTTON_B) {
					_gamePadState.buttonB = buttonEvent.controlInput.held;
				}				
				if (buttonEvent.controlInput.id == ControlButtonsConst.BUTTON_X) {
					_gamePadState.buttonX = buttonEvent.controlInput.held;
				}
				if (buttonEvent.controlInput.id == ControlButtonsConst.BUTTON_Y) {
					_gamePadState.buttonY = buttonEvent.controlInput.held;
				}
			}
			
			if (event.type == ControlEventConst.ON_CONTROLLER_JOYSTICK) {
				var joyEvent:ControllerJoystickEvent = (event as ControllerJoystickEvent);
				_gamePadState.lastInteraction = joyEvent.lastPressedButton.id;
				
				if (joyEvent.joystickControl.id == ControlButtonsConst.BUTTON_LEFTSTICK) {
					_gamePadState.left = joyEvent.joystickControl.left.held;
					_gamePadState.right = joyEvent.joystickControl.right.held;
					_gamePadState.up = joyEvent.joystickControl.up.held;
					_gamePadState.down = joyEvent.joystickControl.down.held;
				}else {
					if (joyEvent.joystickControl.right.held || joyEvent.joystickControl.left.held || joyEvent.joystickControl.up.held || joyEvent.joystickControl.down.held) {
						_gamePadState.rotation = joyEvent.joystickControl.rotation;
					}
					_gamePadState.lastInteraction = ControlButtonsConst.BUTTON_RIGHTSTICK;
				}
			}
			
			if (event.type == ControlEventConst.ON_CONTROLLER_DIRECTIONAL) {
				var dirEvent:ControllerDirectionalEvent = (event as ControllerDirectionalEvent);
				_gamePadState.left = dirEvent.directionalPadControl.left.held;
				_gamePadState.right = dirEvent.directionalPadControl.right.held;
				_gamePadState.up = dirEvent.directionalPadControl.up.held;
				_gamePadState.down = dirEvent.directionalPadControl.down.held;
				
				_gamePadState.lastInteraction = dirEvent.lastPressedButton.id;
			}
			
			if (event.type == ControlEventConst.ON_CONTROLLER_TRIGGER) {
				var triggerEvent:ControllerTriggerEvent = (event as ControllerTriggerEvent);
				_gamePadState.lastInteraction = triggerEvent.trigger.id;
				if (triggerEvent.trigger.id == ControlButtonsConst.BUTTON_LT) {
					_gamePadState.buttonLT = triggerEvent.trigger.held;
				}else {
					_gamePadState.buttonRT = triggerEvent.trigger.held;
				}
			}
			
			_signal.dispatch(_gamePadState);
		}
		
		
		
		public function get signal():Signal {
			return _signal;
		}
		
	}
	
}