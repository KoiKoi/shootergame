package logic.input.state 
{
	/**
	 * ...
	 * @author Der Korser
	 */
	public class GamePadState {
		
		public var lastInteraction:int;
		
		public var gamePadId:String;
		
		public var buttonA:Boolean;
		public var buttonB:Boolean;
		public var buttonX:Boolean;
		public var buttonY:Boolean;
		
		public var buttonRB:Boolean;
		public var buttonLB:Boolean;
		
		public var buttonRT:Boolean;
		public var buttonLT:Boolean;
		
		public var left:Boolean;
		public var right:Boolean;
		public var down:Boolean;
		public var up:Boolean;
		
		public var rotation:Number;
		
		
	}

}