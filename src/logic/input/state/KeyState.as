package logic.input.state {
	/**
	 * ...
	 * @author kk
	 */
	public class KeyState {
		
		private var _keyCode:uint;
		private var _keyPressed:Boolean;
		
		public function KeyState(stateKeyCode:int) {
			_keyCode = stateKeyCode;
		}
		
		public function get keyPressed():Boolean {
			return _keyPressed;
		}
		
		public function set keyPressed(value:Boolean):void {
			_keyPressed = value;
		}
		
		public function get keyCode():int {
			return _keyCode;
		}
		
	}

}