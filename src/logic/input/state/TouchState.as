package logic.input.state {
	import misc.vo.PositionVo;
	/**
	 * ...
	 * @author kk
	 */
	public class TouchState {
		
		public static const MOUSE_RIGHT	:int = 1;
		public static const MOUSE_LEFT	:int = 2;
		public static const MOUSE_MOVE	:int = 3;
		
		private var _lastAction			:int;
		
		private var _leftButtonPressed	:Boolean;
		private var _rightButtonPressed	:Boolean;
		private var _position			:PositionVo;
		
		
		public function TouchState() {
			_leftButtonPressed = false;
			_rightButtonPressed = false;
			_position = new PositionVo(0,0);
		}
		
		public function get leftButtonPressed():Boolean {
			return _leftButtonPressed;
		}
		
		public function get rightButtonPressed():Boolean {
			return _rightButtonPressed;
		}
		
		public function get position():PositionVo {
			return _position;
		}
		
		public function set leftButtonPressed(value:Boolean):void {
			_leftButtonPressed = value;
			_lastAction = MOUSE_LEFT;
		}
		
		public function set rightButtonPressed(value:Boolean):void {
			_rightButtonPressed = value;
			_lastAction = MOUSE_RIGHT;
		}
		
		public function set position(value:PositionVo):void {
			_position = value;
			_lastAction = MOUSE_MOVE;
		}
		
		public function get lastAction():int {
			return _lastAction;
		}
		
	}
}