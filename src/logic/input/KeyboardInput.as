package logic.input 
{
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.utils.Dictionary;
	import logic.input.state.KeyState;
	import org.osflash.signals.Signal;
	/**
	 * ...
	 * @author kk
	 */
	public class KeyboardInput {
		
		public static const KEY_UP:uint = 87;
		public static const KEY_DOWN:uint = 83;
		public static const KEY_LEFT:uint = 65;
		public static const KEY_RIGHT:uint = 68;
		
		
		private var _signal:Signal;
		private var _mainView:Main;
		private var _keyStates:Dictionary;
		
		public function KeyboardInput(main:Main) {
			_mainView = main;
			init();
		}
		
		private function init():void {
			_signal = new Signal();
			_keyStates = new Dictionary();
			
			initKey(KEY_UP);
			initKey(KEY_DOWN);
			initKey(KEY_LEFT);
			initKey(KEY_RIGHT);
			
			_mainView.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyboardDown);
			_mainView.stage.addEventListener(KeyboardEvent.KEY_UP, onKeyboardUp);
			
			_mainView.stage.addEventListener(FocusEvent.KEY_FOCUS_CHANGE, onKeyboardLost);
			_mainView.stage.addEventListener(FocusEvent.FOCUS_OUT, onKeyboardLost);
			_mainView.stage.addEventListener(Event.DEACTIVATE,onDeactivate);
		}
		
		private function onDeactivate(e:Event):void {
			onKeyboardLost();
		}
		
		private function onKeyboardLost(e:FocusEvent=null):void {
			var keyState:KeyState;
			
			keyState = getKeyStateByKeyCode(KEY_UP);
			keyState.keyPressed = false;
			_signal.dispatch(keyState);
			keyState = getKeyStateByKeyCode(KEY_DOWN);
			keyState.keyPressed = false;
			_signal.dispatch(keyState);
			keyState = getKeyStateByKeyCode(KEY_LEFT);
			keyState.keyPressed = false;
			_signal.dispatch(keyState);
			keyState = getKeyStateByKeyCode(KEY_RIGHT);
			keyState.keyPressed = false;
			_signal.dispatch(keyState);
		}
		
		
		private function initKey(keyCode:int):void {
			_keyStates[keyCode] = new KeyState(keyCode);
		}
		
		private function onKeyboardUp(e:KeyboardEvent):void {
			var keyCode:uint = e.keyCode;
			var keyState:KeyState = getKeyStateByKeyCode(keyCode);
			if (keyState) {
				if (keyState.keyPressed == true) {
					keyState.keyPressed = false;
					_signal.dispatch(keyState);
				}
			}
		}
		
		
		private function onKeyboardDown(e:KeyboardEvent):void {
			var keyCode:uint = e.keyCode;
			var keyState:KeyState = getKeyStateByKeyCode(keyCode);
			if (keyState) {
				if (keyState.keyPressed == false) {
					keyState.keyPressed = true;
					_signal.dispatch(keyState);
				}
			}
		}
		
		public function getKeyStateByKeyCode(keyCode:int):KeyState {
			if (_keyStates[keyCode]) {
				return _keyStates[keyCode];
			}
			return null;
		}
		
		public function get signal():Signal {
			return _signal;
		}
		
		
		
		
		
		
	}

}