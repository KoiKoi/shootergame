package 
{

	import flash.display.Sprite;
	import flash.events.Event;
	import logic.InitProcess;
	
	/**
	 * ...
	 * @author Der Korser
	 */
	public class Main extends Sprite {
		
		private var _initProcess:InitProcess;
		
		public function Main():void {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			stage.scaleMode = flash.display.StageScaleMode.NO_SCALE;
			stage.align = flash.display.StageAlign.TOP_LEFT;
			_initProcess = new InitProcess(this);
		}
		
	}
	
}