package misc.vo 
{
	/**
	 * ...
	 * @author kk
	 */
	public class PositionVo {
		
		public var x:int; 
		public var y:int; 
		
		public function PositionVo(xPos:int=0, yPos:int=0) {
			x = xPos;
			y = yPos;
		}
		
	}

}