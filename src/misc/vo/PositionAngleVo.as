package misc.vo 
{
	/**
	 * ...
	 * @author Der Korser
	 */
	public class PositionAngleVo extends PositionVo {
		
		public var rotation:Number;
		
		public function PositionAngleVo(xPos:Number = 0, yPos:Number = 0, rot:Number = 0) {
			x = xPos;
			y = yPos;
			rotation = rot;
		}
		
	}

}