package misc.bitmap {
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class BmpUtil {
		private static const _bdMatrix:Matrix = new Matrix(1, 0, 0, 1);

		public static function getFirstNonTransparentPixelFloodFill(bmd:BitmapData,test_fill_color:uint=0xFFFFFF00) : Point {
			//var hit_bmd:BitmapData=new BitmapData(bmd.width,1,true,0);
			var hit_bmd:BitmapData;
			var m:Matrix=new Matrix();
			for(var i:int=0;i<bmd.height;i++){
				m.ty=-i;
				hit_bmd=new BitmapData(bmd.width,1,true,0);
				hit_bmd.draw(bmd,m);
				hit_bmd.floodFill(0,0,test_fill_color);//use yellow, assume that the image tested is monochrome... (black and transparent)
				var bounds:Rectangle=hit_bmd.getColorBoundsRect(0xFFFFFFFF,test_fill_color);
				if(bounds.width!=bmd.width){
					return new Point(i,bounds.width);
				}
				//hit_bmd.floodFill(0,0,0);
				hit_bmd.dispose();
			}
			return null;
		}

		public static function toBitmapData(from:DisplayObject, transparent:Boolean = true):BitmapData {
			var bounds:Rectangle = from.getBounds(from);
			var bitmapData:BitmapData = new BitmapData(int(bounds.width + 0.5), int(bounds.height + 0.5), transparent, 0);
			_bdMatrix.tx = -bounds.x;
			_bdMatrix.ty = -bounds.y;
			bitmapData.draw(from, _bdMatrix);
			return bitmapData;
		}

		public static function toBitmap(from:DisplayObject):Bitmap {
			return new Bitmap(toBitmapData(from));
		}
	}
}
