package com.bigpoint.utils {
import flash.display.DisplayObject;
import flash.filters.ColorMatrixFilter;

/**
 * @author fsilvestrim
 */
public class ColorHelper {
	static public const GRAYSCALE:Vector.<Number> = Vector.<Number>([0.212671, 0.71516, 0.072169, 0, 0, 0.212671, 0.71516, 0.072169, 0, 0, 0.212671, 0.71516, 0.072169, 0, 0, 0, 0, 0, 1, 0]);
	static public const GRAYSCALE_LIGHT:Vector.<Number> = Vector.<Number>([0.33, 0.33, 0.33, 0, 0, 0.33, 0.33, 0.33, 0, 0, 0.33, 0.33, 0.33, 0, 0, 0, 0, 0, 1, 0]);
	static public const SEPIA:Vector.<Number> = Vector.<Number>([0.3930000066757202, 0.7689999938011169, 0.1889999955892563, 0, 0, 0.3490000069141388, 0.6859999895095825, 0.1679999977350235, 0, 0, 0.2720000147819519, 0.5339999794960022, 0.1309999972581863, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1]);

	/**
	 * @see http://www.adobetutorialz.com/articles/1987/1/Color-Matrix
	 */
	static public function applyColor(p_target:DisplayObject, p_cs:Vector.<Number>):void {
		var matrix:Array = [];
		matrix = matrix.concat([p_cs[0], p_cs[1], p_cs[2], p_cs[3], p_cs[4]]); // red
		matrix = matrix.concat([p_cs[5], p_cs[6], p_cs[7], p_cs[8], p_cs[9]]); // green
		matrix = matrix.concat([p_cs[10], p_cs[11], p_cs[12], p_cs[13], p_cs[14]]); // blue
		matrix = matrix.concat([p_cs[15], p_cs[16], p_cs[17], p_cs[18], p_cs[19]]); // alpha
		p_target.filters = [new ColorMatrixFilter(matrix)];
	}

	static public function removeColor(p_target:DisplayObject):void {
		p_target.filters = [new ColorMatrixFilter()];
	}
}
}
