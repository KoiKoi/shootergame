

package misc.math 
{
	
	/**
	 * Full details: http://gamedev.michaeljameswilliams.com/2009/04/24/angles-in-flash/
	 * Hungarian notation idea: http://www.joelonsoftware.com/articles/Wrong.html
	 * Optimisation credit: http://www.nbilyk.com/optimizing-actionscript-3
	 * 
	 * Use this instead of the Angle functions when you need extra speed
	 * and are going to use the functions multiple times (e.g. over a loop)
	 * 
	 * Usage:	import com.michaeljameswilliams.gamedev.angles.Angle
	 * 			var angleUtil:AngleUtil = new AngleUtil();
	 * 			var degAngle:Number = angleUtil.degFromRad( radAngle );
	 * 
	 * @author MichaelJWilliams
	 */
	public class AngleUtil 
	{
		private static const localPi:Number = Math.PI;
		private static const localTwoPi:Number = Math.PI * 2;
		private static const oneOver180:Number = 1 / 180;
		private static const oneOverPi:Number = 1 / localPi;
		
		/**
		 * @param	p_degInput	Angle, in degrees
		 * @return	Angle, in degrees, in range 0 to 360
		 */
		public static function degFromDeg( p_degInput:Number ):Number
		{
			var degOutput:Number = p_degInput;
			while ( degOutput >= 360 )
			{
				degOutput -= 360;
			}
			while ( degOutput < 0 )
			{
				degOutput += 360;
			}
			return degOutput;
		}
		
		/**
		 * @param	p_rotInput	Angle, in degrees
		 * @return	Angle, in degrees, in range -180 to + 180
		 */
		public static function rotFromRot( p_rotInput:Number ):Number
		{
			var rotOutput:Number = p_rotInput;
			while ( rotOutput > 180 )
			{
				rotOutput -= 360;
			}
			while ( rotOutput < -180 )
			{
				rotOutput += 360;
			}
			return rotOutput;
		}
		
		/**
		 * @param	p_radInput	Angle, in radians
		 * @return	Angle, in radians, in range 0 to ( 2 * pi )
		 */
		public static function radFromRad( p_radInput:Number ):Number
		{
			var radOutput:Number = p_radInput;
			while ( radOutput >= localTwoPi )
			{
				radOutput -= localTwoPi;
			}
			while ( radOutput < -localTwoPi ) 
			{
				radOutput += localTwoPi;
			}
			return radOutput;
		}
		
		/**
		 * @param	p_degInput	Angle, in degrees
		 * @return	Angle, in degrees, in range -180 to +180
		 */
		public static function rotFromDeg( p_degInput:Number ):Number
		{
			var rotOutput:Number = p_degInput;
			while ( rotOutput > 180 )
			{
				rotOutput -= 360;
			}
			while ( rotOutput < -180 )
			{
				rotOutput += 360;
			}
			return rotOutput;
		}
		
		/**
		 * @param	p_rotInput	Angle, in degrees
		 * @return	Angle, in degrees, in range 0 to 360
		 */
		public static function degFromRot( p_rotInput:Number ):Number
		{
			var degOutput:Number = p_rotInput;
			while ( degOutput >= 360 )
			{
				degOutput -= 360;
			}
			while ( degOutput < 0 )
			{
				degOutput += 360;
			}
			return degOutput;
		}
		
		/**
		 * @param	p_radInput	Angle, in radians
		 * @return	Angle, in degrees, in range 0 to 360
		 */
		public static function degFromRad( p_radInput:Number ):Number
		{
			var degOutput:Number = 180 * oneOverPi * radFromRad( p_radInput );
			return degOutput;
		}
		
		/**
		 * @param	p_degInput	Angle, in degrees
		 * @return	Angle, in radians, in range 0 to ( 2 * pi )
		 */
		public static function radFromDeg( p_degInput:Number ):Number
		{
			var radOutput:Number = localPi * oneOver180 * degFromDeg( p_degInput );
			return radOutput;
		}
		
		/**
		 * @param	p_radInput	Angle, in radians
		 * @return	Angle, in degrees, in range -180 to +180
		 */
		public static function rotFromRad( p_radInput:Number ):Number
		{
			return rotFromDeg( degFromRad( p_radInput ) );
		}
		
		/**
		 * @param	p_rotInput	Angle, in degrees
		 * @return	Angle, in radians, in range 0 to ( 2 * pi )
		 */
		public static function radFromRot( p_rotInput:Number ):Number
		{
			return radFromDeg( degFromRot( p_rotInput ) );
		}

	}
	
}