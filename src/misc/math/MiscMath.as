package misc.math 
{
	import configs.constants.StarlingConst;
	/**
	 * ...
	 * @author Der Korser
	 */
	public class MiscMath {
		
		public static function fDistance(x1:Number, y1:Number, x2:Number, y2:Number):Number {
			return (Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));
		}
		
		public static function fAngle(x1:Number, y1:Number, x2:Number, y2:Number):Number {
			return (Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI);
		}
		
		public static function checkNumberBetween(min:Number , value:Number , max:Number):Boolean {
			return min > value ? false : ( max < value ? false : true );
		}
		
		public static  function randomNumber(min:Number, max:Number):Number {
			if (min == max) return min;
			return Math.random() * (max - min) + min;
		}
		public static function getAngle (x1:Number, y1:Number, x2:Number, y2:Number, minus:Number=0):Number {
			var dx:Number = x2 - x1;
			var dy:Number = y2 - y1;
			return Math.atan2(dy,dx) - minus;
		}
		
	}

}