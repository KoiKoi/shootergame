package misc.controller {
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.GameInputEvent;
	import flash.events.KeyboardEvent;
	import flash.ui.GameInput;
	import flash.ui.GameInputDevice;
	import flash.ui.Keyboard;
	import flash.utils.getTimer;
	import misc.controller.event.GameControllerConnectEvent;
	import org.osflash.signals.Signal;
	
	import misc.controller.controller.GameController;
	//import io.arkeus.ouya.controller.OuyaController;
	import misc.controller.controller.Xbox360Controller;

	/**
	 * A class for reading input from controllers. Allows you to pull ready controllers from a queue
	 * of controllers that have been initialized, to allow input from as many controllers as you need.
	 */
	public class ControllerInput {
		
		private var _controllers:Vector.<GameController> = new Vector.<GameController>;
		private var _readyControllers:Vector.<GameController> = new Vector.<GameController>;
		private var _removedControllers:Vector.<GameController> = new Vector.<GameController>;
		private var _gameInput:GameInput;
		private var _signal:Signal;
		
		
		public function ControllerInput():void {
			_gameInput = new GameInput;
			_signal = new Signal();
			_gameInput.addEventListener(GameInputEvent.DEVICE_ADDED, onDeviceAttached);
			_gameInput.addEventListener(GameInputEvent.DEVICE_REMOVED, onDeviceDetached);
		}
		
		/**
		 * Initializes the library, adding event listeners as needed. The passed stage is used to add event
		 * listeners for entering frame and for keyboard events.
		 * 
		 * @param stage A reference to the root flash stage.
		 */
		public function initialize(stage:DisplayObject):void {
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			
			for (var i:uint = 0; i < GameInput.numDevices; i++) {
				attach(GameInput.getDeviceAt(i));
			}
		}
		
		/**
		 * Returns the active controller with the passed index.
		 * 
		 * @param index The index of the controller to grab.
		 * @return An active controller.
		 */
		public function controller(index:uint):GameController {
			return _controllers[index];
		}
		
		/**
		 * Returns the number of active controllers that are connected and taking input.
		 * 
		 * @return The number of active controllers.
		 */
		public function numControllers():uint {
			return _controllers.length;
		}
		
		/**
		 * Returns the number of ready controllers that are connected, but are not yet active.
		 * Use getReadyController() in order to get a ready controller and make it active.
		 * 
		 * @return The number of ready controllers.
		 */
		public function numReadyControllers():uint {
			return _readyControllers.length;
		}
		
		/**
		 * Returns whether or not there is a controller that is ready to be polled for input.
		 * 
		 * @return Whether there is a ready controller or not.
		 */
		public function hasReadyController():Boolean {
			return _readyControllers.length > 0;
		}
		
		/**
		 * Returns a ready controller and activates it (allowing it to be polled for input). This moves the
		 * controller from the "ready controllers" queue to the list of active "controllers".
		 * 
		 * @return The controller, now in a ready state.
		 */
		public function getReadyController():GameController {
			var readyController:GameController = _readyControllers.shift();
			readyController.enable();
			_controllers.push(readyController);
			return readyController;
		}
		
		/**
		 * Returns whether or not one of the currently used controllers has been disconnected. You can check this
		 * queue in order to handle this case gracefully. Also, you can check if the "removed" property of the
		 * controller is true, which also signifies that the controller has been detached from the system and can
		 * no longer be read for input.
		 * 
		 * @return Whether or not there is a detached controller.
		 */
		public function hasRemovedController():Boolean {
			return _removedControllers.length > 0;
		}
		
		/**
		 * Similar to reading a newly ready controller, this allows you to read a removed controller and handle it
		 * however you'd like.
		 * 
		 * @return The removed controller.
		 */
		public function getRemovedController():GameController {
			var removedController:GameController = _removedControllers.shift();
			removedController.disable();
			return removedController;
		}
		
		/**
		 * Callback when a device is attached.
		 * 
		 * @param event The GameInputEvent containing the attached deviced.
		 */
		private function onDeviceAttached(event:GameInputEvent):void {
			attach(event.device);
		}
		
		/**
		 * Attaches a game device by creating a class that corresponds to the device type
		 * and adding it to the ready controllers list.
		 */
		private function attach(device:GameInputDevice):void {
			if (device == null) {
				return;
			}
			var controllerClass:Class = parseControllerType(device.name);
			if (controllerClass == null) {
				// Unknown device
				return;
			}
			
			var controll:GameController = (new controllerClass(device) as GameController);
			
			_readyControllers.push(controll);
			_signal.dispatch(new GameControllerConnectEvent(controll));
		}
		
		/**
		 * Callback when a device is detached.
		 * 
		 * @param event The GameInputEvent containing the detached deviced.
		 */
		private function onDeviceDetached(event:GameInputEvent):void {
			detach(event.device);
		}
		
		/**
		 * Detaches a device by setting the removed attribute to true, removing it from the controllers
		 * list, and adding to the removed controllers list.
		 */
		private function detach(device:GameInputDevice):void {
			if (device == null) {
				return;
			}
			var detachedController:GameController = findAndRemoveDevice(_controllers, device) || findAndRemoveDevice(_readyControllers, device);
			if (detachedController == null) {
				return;
			}
			detachedController.remove();
			_removedControllers.push(detachedController);
		}
		
		/**
		 * Helper method that takes a group and a target device, removes the device from the group
		 * and returns it. If the controller was not present in the group, returns null instead.
		 * 
		 * @param source The group to remove the controller from.
		 * @param target The game input device to remove and return.
		 * @return The removed controller corresponding to the device, or null if it wasn't present.
		 */
		private function findAndRemoveDevice(source:Vector.<GameController>, target:GameInputDevice):GameController {
			var result:GameController = null;
			for each (var controller:GameController in source) {
				if (controller.device == target) {
					result = controller;
					break;
				}
			}
			
			if (result != null) {
				source.splice(source.indexOf(result), 1);
				return result;
			}
			
			return null;
		}
		
		/**
		 * Given the name of a device, returns the supported class for that device. If the device isn't
		 * supported by AS3 Controller Input, returns null.
		 * 
		 * @param name The name of the device.
		 * @return The controller class corresponding to the device name.
		 */
		private function parseControllerType(name:String):Class {
			if (name.toLowerCase().indexOf("xbox 360") != -1) {
				return Xbox360Controller;
			}/* else if (name.toLowerCase().indexOf("ouya") != -1) {
				return OuyaController;
			}*/
			
			return null;
		}
		
		/**
		 * Callback for keyboard events that catches the back and escape keys such that stupid bindings
		 * don't exit the application.
		 * 
		 * @param event The keyboard event.
		 */
		private function onKeyDown(event:KeyboardEvent):void {
			if (event.keyCode == 27 || event.keyCode == Keyboard.BACK) {
				event.preventDefault();
			}
		}
		
		public function get signal():Signal {
			return _signal;
		}
	}
}
