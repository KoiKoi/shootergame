package misc.controller.control {
	import flash.ui.GameInputControl;
	import misc.controller.constants.ControlButtonsConst;
	import misc.controller.control.controlInterface.IGameController;
	import misc.controller.event.ControllerDirectionalEvent;

	import misc.controller.controller.GameController;

	public class DirectionalPadControl implements IGameController {
		
		private var _id:int;
		
		private var _device:GameController;
		private var _up:ButtonControl;
		private var _down:ButtonControl;
		private var _left:ButtonControl;
		private var _right:ButtonControl;
		
		public function DirectionalPadControl(device:GameController, _buttonId:int, up:GameInputControl, down:GameInputControl, left:GameInputControl, right:GameInputControl) {
			_device = device;
			
			_up = new ButtonControl(device, up, ControlButtonsConst.BUTTON_UP);
			_down = new ButtonControl(device, down, ControlButtonsConst.BUTTON_DOWN);
			_left = new ButtonControl(device, left, ControlButtonsConst.BUTTON_LEFT);
			_right = new ButtonControl(device, right, ControlButtonsConst.BUTTON_RIGHT);
			
			_up.signal.add(onPress);
			_down.signal.add(onPress);
			_left.signal.add(onPress);
			_right.signal.add(onPress);
			
			
			_id = _buttonId;
		}
		
		private function onPress(control:GameControl):void {
			_device.controllerSignal.dispatch(new ControllerDirectionalEvent(this, control));
		}
		
		public function reset():void {
			_up.reset();
			_down.reset();
			_left.reset();
			_right.reset();
		}
		
		public function unload():void {
			_up.unload();
			_down.unload();
			_left.unload();
			_right.unload();
		}
		
		public function get controllerId():String {
			return _device.device.id;
		}
		
		public function get type():int {
			return ControlButtonsConst.TYPE_DIRECTIONAL;
		}
		
		public function get id():int {
			return _id;
		}
		
		public function get up():ButtonControl {
			return _up;
		}
		
		public function get down():ButtonControl {
			return _down;
		}
		
		public function get left():ButtonControl {
			return _left;
		}
		
		public function get right():ButtonControl {
			return _right;
		}
		
	}
}
