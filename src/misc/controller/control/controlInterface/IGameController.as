package misc.controller.control.controlInterface {
	/**
	 * ...
	 * @author Der Korser
	 */
	public interface IGameController {
		function reset():void;
		function unload():void;
		function get type():int;
		function get id():int;
		function get controllerId():String;
	}

}