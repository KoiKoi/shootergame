package misc.controller.control {
	import flash.ui.GameInputControl;
	import misc.controller.constants.ControlButtonsConst;

	import misc.controller.controller.GameController;

	public class TriggerControl extends ButtonControl {
		
		public function TriggerControl(device:GameController, control:GameInputControl, buttonId:int) {
			super(device, control, buttonId);
		}
		
		public function get distance():Number {
			return value;
		}
		
		public override function get type():int {
			return ControlButtonsConst.TYPE_TRIGGER;
		}
		
	}
}
