package misc.controller.control {
	import flash.events.Event;
	import flash.ui.GameInputControl;
	import misc.controller.constants.ControlButtonsConst;
	import misc.controller.control.controlInterface.IGameController;
	import misc.controller.event.ControllerButtonEvent;
	import org.osflash.signals.Signal;
	
	import misc.controller.ControllerInput;
	import misc.controller.controller.GameController;

	public class GameControl implements IGameController {
		
		private var _device:GameController;
		private var _control:GameInputControl;
		private var _now:Date;
		private var _value:Number = 0;
		private var _updatedAt:uint = 0;
		private var _signal:Signal;
		private var _id:int;
		
		public function GameControl(dev:GameController, cont:GameInputControl, buttonId:int) {
			_now  = new Date();
			_device = dev;
			_control = cont;
			
			_signal = new Signal();
			_id = buttonId;
			
			if (_control != null) {
				_control.addEventListener(Event.CHANGE, onChange, false, 0, true);
			}
		}
		
		public function reset():void {
			_value = 0;
			_updatedAt = 0;
		}
		
		public function unload():void {
			_control.removeEventListener(Event.CHANGE, onChange);
			_signal.removeAll();
		}
		
		public function get value():Number {
			return _value;
		}
		
		public function get type():int {
			return ControlButtonsConst.TYPE_BUTTON;
		}
		
		public function get id():int {
			return _id;
		}
		
		public function get controllerId():String {
			return _control.device.id;
		}
		
		protected function onChange(event:Event):void {
			_value = (event.target as GameInputControl).value;
			_updatedAt = _now.getTime();
			_signal.dispatch(this);
		}
		
		public function get updatedAt():uint {
			return _updatedAt;
		}
		
		public function get signal():Signal {
			return _signal;
		}
	}
}
