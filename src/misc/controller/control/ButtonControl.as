package misc.controller.control {
	import flash.events.Event;
	import flash.ui.GameInputControl;

	import misc.controller.ControllerInput;
	import misc.controller.controller.GameController;

	public class ButtonControl extends GameControl {
		
		private var _changed:Boolean = false;
		private var _minimum:Number;
		private var _maximum:Number;
		
		public function ButtonControl(device:GameController, control:GameInputControl, buttonId:int, minimum:Number = 0.5, maximum:Number = 1) {
			super(device, control, buttonId);
			this._minimum = minimum;
			this._maximum = maximum;
		}
		
		/*
		public function get pressed():Boolean {
			return updatedAt >= ControllerInput.previous && held && _changed;
		}
		
		public function get released():Boolean {
			return updatedAt >= ControllerInput.previous && !held && _changed;
		}
		*/
		
		public function get pressed():Boolean {
			return held && _changed;
		}
		
		public function get released():Boolean {
			return !held && _changed;
		}
		
		public function get held():Boolean {
			return value >= _minimum && value <= _maximum;
		}
		
		override protected function onChange(event:Event):void {
			var beforeHeld:Boolean = held;
			super.onChange(event);
			_changed = held != beforeHeld;
		}
	}
}
