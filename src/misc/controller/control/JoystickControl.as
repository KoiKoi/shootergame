package misc.controller.control {
	import flash.ui.GameInputControl;
	import misc.controller.constants.ControlButtonsConst;
	import misc.controller.control.controlInterface.IGameController;
	import misc.controller.event.ControllerJoystickEvent;
	import org.osflash.signals.Signal;
	
	import misc.controller.controller.GameController;

	public class JoystickControl implements IGameController {
		private const JOYSTICK_THRESHOLD:Number = 0.5;
		
		private var _id:int;
		
		private var _joystickButton:GameControl;
		private var _xAxis:GameControl;
		private var _yAxis:GameControl;
		
		private var _left:ButtonControl;
		private var _right:ButtonControl;
		private var _up:ButtonControl;
		private var _down:ButtonControl;
		
		private var _reversedY:Boolean;
		
		private var _device:GameController;
		
		
		public function JoystickControl(dev:GameController, buttonId:int, buttonXAxis:GameInputControl, buttonYAxis:GameInputControl, joyButton:GameInputControl, buttonReversedY:Boolean = false) {
			
			_id = buttonId;
			_device = dev;
			
			_joystickButton = new GameControl(dev, joyButton, ControlButtonsConst.BUTTON_JOYSTICK);
			
			_xAxis = new GameControl(dev, buttonXAxis, ControlButtonsConst.BUTTON_JOY_X_AXIS);
			_yAxis = new GameControl(dev, buttonYAxis, ControlButtonsConst.BUTTON_JOY_Y_AXIS);
			
			_left = new ButtonControl(dev, buttonXAxis, ControlButtonsConst.BUTTON_LEFT,  -1, -JOYSTICK_THRESHOLD);
			_right = new ButtonControl(dev, buttonXAxis, ControlButtonsConst.BUTTON_RIGHT, JOYSTICK_THRESHOLD, 1);
			
			if (buttonReversedY) {
				_down = new ButtonControl(dev, buttonYAxis ,ControlButtonsConst.BUTTON_DOWN, JOYSTICK_THRESHOLD, 1);
				_up = new ButtonControl(dev, buttonYAxis, ControlButtonsConst.BUTTON_UP, -1, -JOYSTICK_THRESHOLD);
			} else {
				_up = new ButtonControl(dev, buttonYAxis, ControlButtonsConst.BUTTON_UP, JOYSTICK_THRESHOLD, 1);
				_down = new ButtonControl(dev, buttonYAxis, ControlButtonsConst.BUTTON_DOWN, -1, -JOYSTICK_THRESHOLD);
			}
			
			_joystickButton.signal.add(onPress);
			_xAxis.signal.add(onPress);
			_yAxis.signal.add(onPress);
			
			_reversedY = buttonReversedY;
		}
		
		private function onPress(control:GameControl):void {
			_device.controllerSignal.dispatch(new ControllerJoystickEvent(this, control));
		}
		
		public function get x():Number {
			return _xAxis.value;
		}
		
		public function get controllerId():String {
			return _device.device.id;
		}
		
		
		public function get y():Number {
			return reversedY ? -_yAxis.value : _yAxis.value;
		}
		
		/**
		 * Returns the angle of the joystick in radians.
		 * 
		 * @return The rotation of the joystick in radians.
		 */
		public function get angle():Number {
			return Math.atan2(y, x);
		}
		
		public function unload():void {
			_joystickButton.unload();
			_xAxis.unload();
			_yAxis.unload();
			_left.unload();
			_right.unload();
			_up.unload();
			_down.unload();
		}
		
		public function reset():void {
			_joystickButton.reset();
			_xAxis.reset();
			_yAxis.reset();
			_left.reset();
			_right.reset();
			_up.reset();
			_down.reset();
		}
		
		
		/**
		 * Returns a flash-friendly value for this stick's position in degrees.
		 * 
		 * @return The rotation of the joystick in degrees.
		 */
		public function get rotation():Number {
			return (Math.atan2(-y, x) + (Math.PI / 2)) * 180 / Math.PI;
		}
		
		public function get distance():Number {
			return Math.min(1, Math.sqrt(x * x + y * y));
		}
		
		public function get left():ButtonControl {
			return _left;
		}
		
		public function get right():ButtonControl {
			return _right;
		}
		
		public function get up():ButtonControl {
			return _up;
		}
		
		public function get down():ButtonControl {
			return _down;
		}
		
		public function get reversedY():Boolean {
			return _reversedY;
		}
		
		public function get type():int {
			return ControlButtonsConst.TYPE_STICK;
		}
		
		public function get id():int {
			return _id;
		}
		
		public function get joystickButtons():GameControl {
			return _joystickButton;
		}
		
	}
}
