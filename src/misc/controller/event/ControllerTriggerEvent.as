package misc.controller.event {
	import misc.controller.constants.ControlEventConst;
	import misc.controller.control.GameControl;
	import misc.controller.control.TriggerControl;
	/**
	 * ...
	 * @author Der Korser
	 */
	public class ControllerTriggerEvent extends GameControllerMainEvent {
		
		private var _trigger:TriggerControl;
		
		public function ControllerTriggerEvent(cont:TriggerControl) {
			super(ControlEventConst.ON_CONTROLLER_TRIGGER);
			_trigger = cont;
		}
		
		override public function get controllerId():String {
			return _trigger.controllerId;
		}
		
		public function get trigger():TriggerControl {
			return _trigger;
		}
		
	}

}