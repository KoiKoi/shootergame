package misc.controller.event {
	import misc.controller.constants.ControlEventConst;
	import misc.controller.control.ButtonControl;
	import misc.controller.control.DirectionalPadControl;
	import misc.controller.control.GameControl;
	import misc.controller.control.JoystickControl;
	/**
	 * ...
	 * @author Der Korser
	 */
	public class ControllerJoystickEvent extends GameControllerMainEvent {
		
		private var _joystickControl:JoystickControl;
		private var _lastPressedButton:GameControl;
		
		public function ControllerJoystickEvent(dPad:JoystickControl, lastPressedButton:GameControl) {
			super(ControlEventConst.ON_CONTROLLER_JOYSTICK);
			_joystickControl = dPad;
			_lastPressedButton = lastPressedButton;
		}
		
		override public function get controllerId():String {
			return _joystickControl.controllerId;
		}
		
		public function get lastPressedButton():GameControl {
			return _lastPressedButton;
		}
		
		public function get joystickControl():JoystickControl {
			return _joystickControl;
		}
		
	}

}