package misc.controller.event {
	import misc.controller.constants.ControlEventConst;
	import misc.controller.controller.GameController;
	/**
	 * ...
	 * @author Der Korser
	 */
	public class GameControllerConnectEvent extends GameControllerMainEvent {
		
		private var _controller:GameController;
		
		public function GameControllerConnectEvent(cont:GameController) {
			super(ControlEventConst.ON_CONTROLLER_CONNECT);
			_controller = cont;
		}
		
		override public function get controllerId():String {
			return _controller.device.id;
		}
		
		public function get controller():GameController {
			return _controller;
		}
		
	}

}