package misc.controller.event {
	/**
	 * ...
	 * @author Der Korser
	 */
	public class GameControllerMainEvent {
		
		protected var _type:String = "";
		protected var _controllerId:String = "";
		
		public function GameControllerMainEvent(eventType:String) {
			_type = eventType;
		}
		
		public function get type():String {
			return _type;
		}
		
		public function get controllerId():String {
			return _controllerId;
		}
		
	}

}