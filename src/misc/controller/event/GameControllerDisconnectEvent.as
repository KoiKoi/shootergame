package misc.controller.event {
	import misc.controller.constants.ControlEventConst;
	import misc.controller.controller.GameController;
	/**
	 * ...
	 * @author Der Korser
	 */
	public class GameControllerDisconnectEvent extends GameControllerMainEvent {
		
		private var _controller:GameController;
		
		public function GameControllerDisconnectEvent(cont:GameController) {
			super(ControlEventConst.ON_CONTROLLER_DISCONNECT, cont.device.id);
			_controller = cont;
		}
		
		override public function get controllerId():String {
			return _controller.device.id;
		}
		
		public function get controller():GameController {
			return _controller;
		}
		
	}

}