package misc.controller.event {
	import misc.controller.constants.ControlEventConst;
	import misc.controller.control.ButtonControl;
	import misc.controller.control.DirectionalPadControl;
	import misc.controller.control.GameControl;
	/**
	 * ...
	 * @author Der Korser
	 */
	public class ControllerDirectionalEvent extends GameControllerMainEvent {
		
		private var _directionalPadControl:DirectionalPadControl;
		private var _lastPressedButton:GameControl;
		
		public function ControllerDirectionalEvent(dPad:DirectionalPadControl, lastPressedButton:GameControl) {
			super(ControlEventConst.ON_CONTROLLER_DIRECTIONAL);
			_directionalPadControl = dPad;
			_lastPressedButton = lastPressedButton;
		}
		
		override public function get controllerId():String {
			return _directionalPadControl.controllerId;
		}
		
		public function get directionalPadControl():DirectionalPadControl {
			return _directionalPadControl;
		}
		
		public function get lastPressedButton():GameControl {
			return _lastPressedButton;
		}
		
	}

}