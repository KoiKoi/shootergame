package misc.controller.event {
	import misc.controller.constants.ControlEventConst;
	import misc.controller.control.ButtonControl;
	import misc.controller.control.GameControl;
	import misc.controller.controller.GameController;
	/**
	 * ...
	 * @author Der Korser
	 */
	public class ControllerButtonEvent extends GameControllerMainEvent {
		
		private var _button:ButtonControl;
		
		public function ControllerButtonEvent(cont:ButtonControl) {
			super(ControlEventConst.ON_CONTROLLER_BUTTON);
			_button = cont;
		}
		
		override public function get controllerId():String {
			return _button.controllerId;
		}
		
		public function get controlInput():ButtonControl {
			return _button;
		}
		
	}

}