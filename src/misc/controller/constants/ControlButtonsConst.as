package misc.controller.constants {
	/**
	 * ...
	 * @author kk
	 */
	public class ControlButtonsConst {
			
			public static const TYPE_BUTTON:int = 0;
			public static const TYPE_STICK:int = 1;
			public static const TYPE_TRIGGER:int = 2;
			public static const TYPE_DIRECTIONAL:int = 3;
			
			public static const BUTTON_A:int = 0;
			public static const BUTTON_B:int = 1;
			public static const BUTTON_X:int = 2;
			public static const BUTTON_Y:int = 3;
			
			public static const BUTTON_LB:int = 4;
			public static const BUTTON_RB:int = 5;
			
			public static const BUTTON_LT:int = 6;
			public static const BUTTON_RT:int = 7;
			
			public static const BUTTON_LEFTSTICK:int = 8;
			public static const BUTTON_RIGHTSTICK:int = 9;
			
			public static const BUTTON_DPAD:int = 10;
			
			public static const BUTTON_BACK:int = 11;
			public static const BUTTON_START:int = 12;
			
			public static const BUTTON_LEFT:int = 13;
			public static const BUTTON_RIGHT:int = 14;
			
			public static const BUTTON_UP:int = 15;
			public static const BUTTON_DOWN:int = 16;
			
			public static const BUTTON_JOYSTICK:int = 17;
			
			public static const BUTTON_JOY_X_AXIS:int = 18;
			public static const BUTTON_JOY_Y_AXIS:int = 19;
	}

}