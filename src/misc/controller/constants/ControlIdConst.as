package misc.controller.constants {
	/**
	 * ...
	 * @author kk
	 */
	public class ControlIdConst {
		
		public static const BUTTON_1	:String = 'BUTTON_1';
		public static const BUTTON_2	:String = 'BUTTON_2';
		public static const BUTTON_3	:String = 'BUTTON_3';
		public static const BUTTON_4	:String = 'BUTTON_4';
		
		public static const BUTTON_5	:String = 'BUTTON_5';
		public static const BUTTON_6	:String = 'BUTTON_6';
		public static const BUTTON_7	:String = 'BUTTON_7';
		public static const BUTTON_8	:String = 'BUTTON_8';
		public static const BUTTON_9	:String = 'BUTTON_9';
		public static const BUTTON_10	:String = 'BUTTON_10';
		public static const BUTTON_11	:String = 'BUTTON_11';
		public static const BUTTON_12	:String = 'BUTTON_12';
		public static const BUTTON_13	:String = 'BUTTON_13';
		public static const BUTTON_14	:String = 'BUTTON_14';
		public static const BUTTON_15	:String = 'BUTTON_15';
		public static const BUTTON_16	:String = 'BUTTON_16';
		public static const BUTTON_17	:String = 'BUTTON_17';
		public static const BUTTON_18	:String = 'BUTTON_18';
		public static const BUTTON_19	:String = 'BUTTON_19';
		
		public static const AXIS_0		:String = 'AXIS_0';
		public static const AXIS_1		:String = 'AXIS_1';
		public static const AXIS_2		:String = 'AXIS_2';
		public static const AXIS_3		:String = 'AXIS_3';
		
	}

}