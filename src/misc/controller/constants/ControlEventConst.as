package misc.controller.constants {
	/**
	 * ...
	 * @author Der Korser
	 */
	public class ControlEventConst {
		
		public static const ON_CONTROLLER_CONNECT:String = "ON_CONTROLLER_CONNECT";
		public static const ON_CONTROLLER_DISCONNECT:String = "ON_CONTROLLER_DISCONNECT";
		
		public static const ON_CONTROLLER_BUTTON:String = "ON_CONTROLLER_BUTTON";
		static public const ON_CONTROLLER_DIRECTIONAL:String = "ON_CONTROLLER_DIRECTIONAL";
		static public const ON_CONTROLLER_JOYSTICK:String = "ON_CONTROLLER_JOYSTICK";
		static public const ON_CONTROLLER_TRIGGER:String = "ON_CONTROLLER_TRIGGER";
		
	}

}