package misc.controller.controller {
	import flash.ui.GameInputControl;
	import flash.ui.GameInputDevice;
	import flash.utils.Dictionary;
	import misc.controller.control.ButtonControl;
	import misc.controller.control.controlInterface.IGameController;
	import misc.controller.control.DirectionalPadControl;
	import misc.controller.control.GameControl;
	import misc.controller.control.JoystickControl;
	import misc.controller.control.TriggerControl;
	import misc.controller.event.ControllerButtonEvent;
	import misc.controller.event.ControllerTriggerEvent;
	import org.osflash.signals.natives.base.SignalBitmap;
	import org.osflash.signals.Signal;

	/**
	 * A class abstracting away the input controls for a single controller.
	 */
	public class GameController {
		
		/** The underlying source game device. */
		public var device:GameInputDevice;
		
		/** A flag indicating if this controller was removed (no longer usable). */
		public var removed:Boolean = false;
		
		protected var inputs:Dictionary; // of GameControl
		protected var controlMap:Dictionary; // of GameInputControl
		
		protected var _controllerSignal:Signal;
		
		/** Creates a game controller and binds the controlers to the source device. */
		public function GameController(device:GameInputDevice) {
			
			inputs = new Dictionary();
			controlMap = new Dictionary();
			
			_controllerSignal = new Signal();
			
			this.device = device;
			
			for (var i:uint = 0; i < device.numControls; i++) {
				var control:GameInputControl = device.getControlAt(i);
				controlMap[control.id] = control;
			}
			
			bindControls();
		}
		
		/**
		 * Sets the enabled flag to true.
		 */
		public function enable():void {
			device.enabled = true;
		}
		
		/**
		 * Sets the enabled flag to false.
		 */
		public function disable():void {
			device.enabled = true;
		}
		
		/**
		 * Sets the controller as removed.
		 */
		public function remove():void {
			removed = true;
			unload();
		}
		
		/**
		 * SetButton
		 */
		
		protected function addDPad(buttonControll:String, buttonControll2:String, buttonControll3:String, buttonControll4:String, buttonId:int):void {
			var dirPad:DirectionalPadControl;
			dirPad = new DirectionalPadControl(this, buttonId, controlMap[buttonControll], controlMap[buttonControll2], controlMap[buttonControll3], controlMap[buttonControll4]);
			inputs[buttonId] = dirPad;
		}
		
		protected function addButton(buttonControll:String, buttonId:int):void {
			var button:ButtonControl;
			button = new ButtonControl(this, controlMap[buttonControll], buttonId);
			button.signal.add(onPressButton);
			inputs[buttonId] = button;
		}
		
		protected function addTrigger(buttonControll:String, buttonId:int):void {
			var trigger:TriggerControl;
			trigger = new TriggerControl(this, controlMap[buttonControll], buttonId);
			trigger.signal.add(onPressTrigger);
			inputs[buttonId] = trigger;
		}
		
		protected function addStickControl(buttonControll:String, buttonControll2:String, buttonControll3:String, buttonId:int):void {
			var joyPad:JoystickControl;
			joyPad = new JoystickControl(this, buttonId, controlMap[buttonControll], controlMap[buttonControll2], controlMap[buttonControll3])
			inputs[buttonId] = joyPad;
		}
		
		private function onPressButton(control:ButtonControl):void {
			_controllerSignal.dispatch(new ControllerButtonEvent(control));
		}

		private function onPressTrigger(control:TriggerControl):void {
			_controllerSignal.dispatch(new ControllerTriggerEvent(control));
		}
		
		public function getButton(buttonId:int):ButtonControl {
			return inputs[buttonId] as ButtonControl;
		}
		
		/**
		 * Resets all the inputs on the controller. Useful after a state change when you don't
		 * want the inputs to trigger in the new state.
		 */
		public function unload():void {
			for each (var value:IGameController in inputs) {
				value.unload();
			}
		}
		
		/**
		 * Resets all the inputs on the controller. Useful after a state change when you don't
		 * want the inputs to trigger in the new state.
		 */
		public function reset():void {
			for each (var value:IGameController in inputs) {
				value.reset();
			}
		}
		
		/**
		 * Private method that is called on initialization. All control bindings should
		 * go in this method.
		 */
		protected function bindControls():void {
			throw new Error("You must implement bindControls in each GameController subclass");
		}
		
		public function get controllerSignal():Signal {
			return _controllerSignal;
		}
		
		
	}
}
