package misc.controller.controller {
	import flash.ui.GameInputControl;
	import flash.ui.GameInputDevice;
	import flash.utils.Dictionary;
	import misc.controller.constants.ControlButtonsConst;
	import misc.controller.constants.ControlIdConst;
	import misc.controller.control.GameControl;
	

	/**
	 * A class containing the bindings for a single Xbox 360 controller.
	 */
	public class Xbox360Controller extends GameController {
		
		public function Xbox360Controller(device:GameInputDevice) {
			super(device);
		}
		
		override protected function bindControls():void {
			
			addButton(ControlIdConst.BUTTON_4, ControlButtonsConst.BUTTON_A); //a
			addButton(ControlIdConst.BUTTON_5, ControlButtonsConst.BUTTON_B); //b
			addButton(ControlIdConst.BUTTON_6, ControlButtonsConst.BUTTON_X); //x
			addButton(ControlIdConst.BUTTON_7, ControlButtonsConst.BUTTON_Y); //y
			
			addButton(ControlIdConst.BUTTON_8, ControlButtonsConst.BUTTON_LB); //lb
			addButton(ControlIdConst.BUTTON_9, ControlButtonsConst.BUTTON_RB); //rb
			
			addTrigger(ControlIdConst.BUTTON_10, ControlButtonsConst.BUTTON_LT); //lt
			addTrigger(ControlIdConst.BUTTON_11, ControlButtonsConst.BUTTON_RT); //rt
			
			addStickControl(ControlIdConst.AXIS_0, ControlIdConst.AXIS_1, ControlIdConst.BUTTON_14, ControlButtonsConst.BUTTON_LEFTSTICK);
			addStickControl(ControlIdConst.AXIS_2, ControlIdConst.AXIS_3, ControlIdConst.BUTTON_15, ControlButtonsConst.BUTTON_RIGHTSTICK);
			
			addDPad(ControlIdConst.BUTTON_16, ControlIdConst.BUTTON_17, ControlIdConst.BUTTON_18, ControlIdConst.BUTTON_19, ControlButtonsConst.BUTTON_DPAD);
			
			addButton(ControlIdConst.BUTTON_12, ControlButtonsConst.BUTTON_BACK); //back
			addButton(ControlIdConst.BUTTON_13, ControlButtonsConst.BUTTON_START); //start
		}
		
	}
}
