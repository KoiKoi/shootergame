package playfield 
{
	import flash.text.TextFormatAlign;
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.text.TextField;
	/*import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	*/
	
	/**
	 * ...
	 * @author kk
	 */
	public class PlayfieldHud extends Sprite {
		
		private static const HEALTH_TEXT:String = 'Health: '
		private static const ROUND_TEXT:String = 'Round: '
		
		private var _healthText:TextField;
		private var _roundText:TextField;
		
		//private var _textHealthFormt:TextFormat;
		//private var _textRoundFormt:TextFormat;
		public function PlayfieldHud() {
			
			touchable = false;
			
			_healthText = new TextField(300, 20, '');
			_healthText.hAlign = TextFormatAlign.LEFT;
			_healthText.text = PlayfieldHud.HEALTH_TEXT;
			addChild(_healthText);
			
			_roundText = new TextField(300, 20, '');
			_roundText.hAlign = TextFormatAlign.LEFT;
			_roundText.text = PlayfieldHud.ROUND_TEXT;
			_roundText.y = _healthText.height;
			addChild(_roundText);
			
		}
		
		public function gameOver():void {
			_healthText.fontSize = 50;
			_healthText.text = "GAME OVER ! " + _roundText.text;
			_roundText.text = "";
			//_healthText.setTextFormat(_textHealthFormt);
		}
		
		public function set currentHealth(value:int):void {
			_healthText.text = PlayfieldHud.HEALTH_TEXT + String(value);
			//_healthText.setTextFormat(_textHealthFormt);
		}
		
		public function set currentRound(value:int):void {
			_roundText.text = PlayfieldHud.ROUND_TEXT + String(value);
			//_roundText.setTextFormat(_textRoundFormt);
		}
		
	}

}