package playfield {
	import configs.constants.GameObjectTypes;
	import configs.MainConfig;
	import configs.gameObjects.module.BulletSetupModule;
	import configs.gameObjects.module.EnemySetupModule;
	import configs.gameObjects.module.WeaponSetupModule;
	import graphics.GraphicLibrary;
	import org.osflash.signals.Signal;
	import playfield.gameItems.BulletGameObject;
	import playfield.gameItems.EnemyGameObject;
	import playfield.gameItems.HeroGameObject;
	import playfield.gameItems.MainGameObject;
	import playfield.gameItems.WeaponGameObject;
	import starling.display.Sprite;
	/**
	 * ...
	 * @author Der Korser
	 */
	public class GameObjectManager {
		
		private var _graphicLib:GraphicLibrary;
		private var _objectPool:GameObjectPool;
		private var _mainConfig:MainConfig;
		private var _playfieldSignal:Signal;
		
		public function GameObjectManager(config:MainConfig, signal:Signal) {
			_mainConfig = config;
			_playfieldSignal = signal;
			
			//Init instances
			_objectPool = new GameObjectPool();
			_graphicLib = new GraphicLibrary();
		}
		
		public function removeGameObjectById(gameObjectId:int):void {
			_objectPool.removeGameObjectById(gameObjectId);
		}
		
		public function createWeapon(weaponType:int):WeaponGameObject {
			
			var weapon:WeaponGameObject = _objectPool.getDeletedWeapon(weaponType);
			
			if (weapon) {
				weapon.reInit();
			} else {
				var weaponModul:WeaponSetupModule = _mainConfig.getWeaponModuleByType(weaponType);
				weapon = new WeaponGameObject(weaponModul, _graphicLib.getGraphic(weaponModul.configVo.graphicId), _playfieldSignal);
				_objectPool.registerObject(weapon);
			}
			
			return weapon;
		}
		
		public function createBullet(bulletType:int):BulletGameObject {
			
			var bullet:BulletGameObject = _objectPool.getDeletedBullet(bulletType);
			
			if (bullet) {
				bullet.reInit();
			} else {
				var bulletModul:BulletSetupModule = _mainConfig.getBulletModulesByType(bulletType);
				bullet = new BulletGameObject(bulletModul, _graphicLib.getGraphic(bulletModul.configVo.graphicId), _playfieldSignal);
				_objectPool.registerObject(bullet);
			}
			
			return bullet;
		}
		
		public function createHero():HeroGameObject  {
			
			//Create a new Hero
			var sprite:Sprite = new Sprite();
			sprite.addChild(_graphicLib.getGraphic(_mainConfig.playerConfig.graphicId));
			var player:HeroGameObject = new HeroGameObject(_mainConfig.playerConfig, sprite, _playfieldSignal);
			player.x = 800;
			player.y = 800;
			
			
			//Startup Weapons
			var weapon:WeaponGameObject;
			
			if (_mainConfig.playerConfig.primaryWeaponType != -1) {
				weapon = createWeapon(_mainConfig.playerConfig.primaryWeaponType);
				player.addWeapon(weapon,HeroGameObject.WEAPON_PRIMARY);
			}
			
			if (_mainConfig.playerConfig.secondaryWeaponType != -1) {
				weapon = createWeapon(_mainConfig.playerConfig.secondaryWeaponType);
				player.addWeapon(weapon, HeroGameObject.WEAPON_SECONDARY);
			}
			
			return player;
		}
		
		public function createEnemy(enemyType:int):EnemyGameObject {
			var enemy:EnemyGameObject = _objectPool.getDeletedEnemy(enemyType);
			
			if (enemy) {
				enemy.reInit();
			} else {
				var enemyModul:EnemySetupModule = _mainConfig.getEnemyModulesByType(enemyType);
				enemy = new EnemyGameObject(enemyModul, _graphicLib.getGraphic(enemyModul.configVo.graphicId), _playfieldSignal);
				_objectPool.registerObject(enemy);
			}
			
			return enemy;
		}
		
		
		public function get objectPool():GameObjectPool {
			return _objectPool;
		}
		
		public function get mainConfig():MainConfig {
			return _mainConfig;
		}
		
		public function get graphicLib():GraphicLibrary {
			return _graphicLib;
		}
		
	}

}