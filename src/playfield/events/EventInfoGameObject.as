package playfield.events 
{
	/**
	 * ...
	 * @author kk
	 */
	public class EventInfoGameObject extends EventGameObject {
		
		public var health:int;
		public var gameObjectId:Number;
		public var gameObjectType:int;
		
		public function EventInfoGameObject(infoHealth:int=-1, infoObjectId:Number=-1, infoObjectType:int=-1) {
			_eventType = EventGameObject.INFO_GAME_OBJECT;
			health = infoHealth;
			gameObjectId = infoObjectId;
			gameObjectType = infoObjectType;
		}
		
	}

}