package playfield.events 
{
	import adobe.utils.CustomActions;
	import misc.vo.PositionAngleVo;
	/**
	 * ...
	 * @author kk
	 */
	public class EventRemoveGameObject extends EventGameObject {
		
		public var remitterId:int;
		public var targetId:int;
		public var targetType:int;
		
		public function EventRemoveGameObject(createObjectType:int=-1, objectId:int=-1, objectType:int=-1) {
			_eventType = EventGameObject.REMOVE_GAME_OBJECT;
			remitterId = createObjectType;
			targetId = objectId;
			targetType = objectType;
		}
		
		
	}

}