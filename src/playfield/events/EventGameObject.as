package playfield.events 
{
	/**
	 * ...
	 * @author kk
	 */
	public class EventGameObject {
		
		public static const CREATE_GAME_OBJECT			:String = 'CREATE_GAME_OBJECT';
		public static const REMOVE_GAME_OBJECT			:String = 'REMOVE_GAME_OBJECT';
		public static const INFO_GAME_OBJECT			:String = 'INFO_GAME_OBJECT';
		
		protected var _eventType:String;
		
		public function EventGameObject() {
			
		}
		
		public function get eventType():String {
			return _eventType;
		}
		
	}

}