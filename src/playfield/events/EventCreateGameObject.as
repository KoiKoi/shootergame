package playfield.events 
{
	import adobe.utils.CustomActions;
	import misc.vo.PositionAngleVo;
	/**
	 * ...
	 * @author kk
	 */
	public class EventCreateGameObject extends EventGameObject {
		
		public var remitterId:int;
		
		public var objectType:int;
		public var subType:int;
		public var createPosAngle:PositionAngleVo;
		
		public function EventCreateGameObject(createObjectType:int=-1, createSubType:int=-1, createPosition:PositionAngleVo=null) {
			_eventType = EventGameObject.CREATE_GAME_OBJECT;
			objectType = createObjectType;
			subType = createSubType;
			createPosAngle = createPosition;
		}
		
		
	}

}