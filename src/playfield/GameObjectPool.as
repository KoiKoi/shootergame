package playfield 
{
	import configs.constants.GameObjectTypes;
	import flash.utils.Dictionary;
	import playfield.gameItems.BulletGameObject;
	import playfield.gameItems.EnemyGameObject;
	import playfield.gameItems.MainGameObject;
	import playfield.gameItems.WeaponGameObject;
	/**
	 * ...
	 * @author kk
	 */
	public class GameObjectPool {
		
		private var _gameObjects:Dictionary;
		private var _gameActiveObjectIds:Dictionary;
		private var _gameDeletedObjectIds:Dictionary;
		
		private var _idCounter:Number;
		
		public function GameObjectPool() {
			
			//All GameObjects By Unigue-Id
			_gameObjects = new Dictionary();
			
			//All Active GameObjectIds By Object-Type -> Unigue-Id Vector
			_gameActiveObjectIds = new Dictionary();
			
			//All Deactive GameObjectIds By Object-Type, Sub-Type -> Unigue-Id Vector
			_gameDeletedObjectIds = new Dictionary();
			
			_idCounter = 0;
			
			initObjectType(GameObjectTypes.BULLET_TYPE);
			initObjectType(GameObjectTypes.ENEMY_TYPE);
			initObjectType(GameObjectTypes.WEAPON_TYPE);
			
		}
		
		public function initObjectType(objectType:int):void {
			if (_gameActiveObjectIds[objectType] == null) {
				_gameActiveObjectIds[objectType] = new Vector.<int>();
			}
			if (_gameDeletedObjectIds[objectType] == null) {
				_gameDeletedObjectIds[objectType] = new Dictionary(); // Dictionary of Sub-Type
			}
		}
		
		
		
		public function getDeletedWeapon(type:int):WeaponGameObject {
			//Get Item From Pool
			var uniqueId:MainGameObject = getGameObjectFromDeletePool(GameObjectTypes.WEAPON_TYPE, type);
			if (uniqueId) {
				return (uniqueId as WeaponGameObject);
			}
			return null;
		}
		
		public function getDeletedBullet(type:int):BulletGameObject {
			//Get Item From Pool
			var uniqueId:MainGameObject = getGameObjectFromDeletePool(GameObjectTypes.BULLET_TYPE, type);
			if (uniqueId) {
				return (uniqueId as BulletGameObject);
			}
			return null;
		}
		
		public function getDeletedEnemy(type:int):EnemyGameObject {
			//Get Item From Pool
			var uniqueId:MainGameObject = getGameObjectFromDeletePool(GameObjectTypes.ENEMY_TYPE, type);
			if (uniqueId) {
				return (uniqueId as EnemyGameObject);
			}
			return null;
		}
		
		public function getGameObjectById(id:int):MainGameObject {
			return _gameObjects[id];
		}
		
		public function registerObject(gameObject:MainGameObject):void {
			if (gameObject.id == -1) {
				gameObject.id = getNextId();
				_gameObjects[gameObject.id] = gameObject;
				setIdToActivePool(gameObject.gameObjectType, gameObject.id);
			}
		}
		
		
		
		public function getActiveGameObjectIdsByType(objectType:int):Vector.<int> {
			var vector:Vector.<int>;
			
			if (_gameActiveObjectIds[objectType] == null) {
				_gameActiveObjectIds[objectType] = new Vector.<int>();
			}
			
			return _gameActiveObjectIds[objectType];
		}
		
		
		public function removeGameObjectById(uniqueId:int):void {
			var gameObject:MainGameObject = getGameObjectById(uniqueId);
			gameObject.visible = false;
			setIdToDelPool(gameObject.gameObjectType, gameObject.subType, gameObject.id);
			removeIdFromActivePool(gameObject.gameObjectType, gameObject.id);
		}
		
		private function removeIdFromActivePool(gameObjectType:int, uniqueId:Number):void {
			var activeVec:Vector.<int> = getVectorFromActPool(gameObjectType);
			
			var activeVecLen:int = activeVec.length;
			for (var i:int = 0; i < activeVecLen; i++) {
				if (activeVec[i] == uniqueId) {
					activeVec.splice(i, 1);
					return;
				}
			}
		}
		
		private function setIdToActivePool(gameObjectType:int, uniqueId:Number):void {
			_gameActiveObjectIds[gameObjectType].push(uniqueId);
		}
		
		private function getGameObjectFromDeletePool(objectType:int, subTypeId:int):MainGameObject {
			var gameObject:MainGameObject;
			var uniqueId:int = getIdFromDelPool(objectType, subTypeId);
			if (uniqueId != -1) {
				setIdToActivePool(objectType, uniqueId);
				gameObject = (_gameObjects[uniqueId] as MainGameObject);
				gameObject.reInit();
				gameObject.visible = true;
			}
			return gameObject;
		}
		
		private function getIdFromDelPool(objectType:int, subTypeId:int):int {
			var uniqueNr:int = -1;
			
			if (_gameDeletedObjectIds[objectType][subTypeId] == null) {
				return uniqueNr;
			} 
			var delVec:Vector.<int> = getVectorFromDelPool(objectType, subTypeId);
			
			if (delVec.length > 0) {
				uniqueNr = delVec.shift();
			}
			return uniqueNr;
		}
		
		private function getVectorFromDelPool(objectType:int, subTypeId:int):Vector.<int> {
			return _gameDeletedObjectIds[objectType][subTypeId] as Vector.<int>;
		}
		
		private function getVectorFromActPool(objectType:int):Vector.<int> {
			return _gameActiveObjectIds[objectType] as Vector.<int>;
		}
		
		private function setIdToDelPool(objectType:int, subTypeId:int, uniqueId:Number):void {
			if (_gameDeletedObjectIds[objectType][subTypeId] == null) {
				_gameDeletedObjectIds[objectType][subTypeId] = new Vector.<int>(); // Dictionary of Ids
			}
			
			var delVec:Vector.<int> = getVectorFromDelPool(objectType, subTypeId);
			var delVecLen:int = delVec.length;
			for (var i:int = 0; i < delVecLen; i++) {
				if (delVec[i] == uniqueId) {
					return;
				}
			}
			
			_gameDeletedObjectIds[objectType][subTypeId].push(uniqueId);
		}
		
		public function getNextId():int {
			return _idCounter++;
		}
		
		
	}

}