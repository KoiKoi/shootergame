package playfield.gameItems {
	import adobe.utils.CustomActions;
	import configs.constants.GameObjectTypes;
	import configs.constants.StarlingConst;
	import configs.gameObjects.vo.PlayerConfigVo;
	import misc.math.AngleUtil;
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import flash.utils.Dictionary;
	import misc.math.MiscMath;
	import misc.vo.PositionVo;
	import org.osflash.signals.Signal;
	import playfield.gameItems.logicModules.weapon.shoot.WeaponSingleShot;
	/**
	 * ...
	 * @author kk
	 */
	public class HeroGameObject extends MainGameObject {
		
		public static const WEAPON_PRIMARY		:int = 1;
		public static const WEAPON_SECONDARY	:int = 2;
		
		public static const MOVE_DIRECTION_LEFT	:int = 1;
		public static const MOVE_DIRECTION_RIGHT:int = 2;
		public static const MOVE_DIRECTION_UP	:int = 3;
		public static const MOVE_DIRECTION_DOWN	:int = 4;
		
		private var _activeButtons	:Vector.<int>;
		
		private var _xMove			:Number;
		private var _yMove			:Number;
		
		private var _energy	:int;
		
		private var _configVo		:PlayerConfigVo;
		private var _weaponSlot		:Dictionary;
		
		public function HeroGameObject(config:PlayerConfigVo, heroGraphic:DisplayObject, signal:Signal) {
			super(signal, GameObjectTypes.HERO_TYPE, 0);
			_configVo = config;
			init();
			changeMainGraphic(heroGraphic);
		}
		
		
		private function init():void {
			_energy = _configVo.maxEnergy;
			_weaponSlot = new Dictionary();
			_activeButtons = new Vector.<int>();
			_xMove = 0;
			_yMove = 0;
		}
		
		public function addWeapon(weapon:WeaponGameObject, slot:int):void {
			var isFire:Boolean = removeWeapon(slot);
			(mainGraphic as Sprite).unflatten();
			weapon.x = (slot == WEAPON_PRIMARY) ? (-20) : 20;
			weapon.y = 0;
			weapon.rotation = 0;
			weapon.setOwner(this);
			weapon.fire(isFire);
			(mainGraphic as Sprite).addChild(weapon.mainGraphic);
			_weaponSlot[slot] = weapon;
			(mainGraphic as Sprite).flatten();
		}
		
		public function removeWeapon(slot:int):Boolean {
			(mainGraphic as Sprite).unflatten();
			var isFire:Boolean;
			var weapon:WeaponGameObject = getWeaponBySlotId(slot);
			if (weapon) {
				isFire = weapon.isPressFire;
				weapon.fire(false);
				weapon.setOwner(null);
				if ((mainGraphic as Sprite).contains(weapon.mainGraphic)) {
					(mainGraphic as Sprite).removeChild(weapon.mainGraphic);
				}
				weapon.removeMe();
				delete _weaponSlot[slot];
			}
			(mainGraphic as Sprite).flatten();
			return isFire;
		}
		
		
		public function updateMove(moveDirection:int, go:Boolean):void {
			var len:int;
			var mdir:int;
			var i:int;
			
			if (go) {
				_activeButtons.push(moveDirection);
			}else {
				for (i = 0; i < _activeButtons.length; i++) {
					if (_activeButtons[i] == moveDirection) {
						_activeButtons.splice(i, 1);
					}
				}
			}
			
			len = _activeButtons.length;
			_xMove = 0;
			_yMove = 0;
			
			for (i = 0; i < len; i++) {
				mdir = _activeButtons[i];
				if (mdir == HeroGameObject.MOVE_DIRECTION_LEFT) {
					_xMove = (_configVo.maxSpeed * -1);
				}else if (mdir == HeroGameObject.MOVE_DIRECTION_RIGHT) {
					_xMove = _configVo.maxSpeed;
				}
				
				if (mdir == HeroGameObject.MOVE_DIRECTION_UP) {
					_yMove = (_configVo.maxSpeed*-1);
				}else if (mdir == HeroGameObject.MOVE_DIRECTION_DOWN) {
					_yMove = _configVo.maxSpeed;
				}
			}
			
		}
		
		public override function update():void {
			this.y += _yMove;
			this.x += _xMove;
		}
		
		
		public function weaponInteraction(slot:int, doFire:Boolean):void {
			if (getWeaponBySlotId(slot)) {
				getWeaponBySlotId(slot).fire(doFire);
			}
		}
		
		
		override public function deactivate():void {
			super.deactivate();
			weaponInteraction(HeroGameObject.WEAPON_PRIMARY, false);
			weaponInteraction(HeroGameObject.WEAPON_SECONDARY, false);
		}
		
		public function getWeaponBySlotId(slotId:int):WeaponGameObject {
			if (_weaponSlot[slotId]) {
				return _weaponSlot[slotId];
			}
			return null;
		}
		
		public function turn(mousePosition:PositionVo):void {
			this.rotation = MiscMath.getAngle(mousePosition.x,mousePosition.y,this.x,this.y,StarlingConst.STARLING_90_DEG);
		}
		
		public function damage(value:int):void {
			_energy -= value;
			if ((_energy <= 0) && (isAlive)) {
				_energy = 0;
				infoGameObject(_energy);
				removeMe();
			}else {
				infoGameObject(_energy);
			}
		}
		
		public function changeDegrees(rotation:Number):void {
			this.rotation = AngleUtil.radFromDeg(rotation);
		}
		
		public function get configVo():PlayerConfigVo {
			return _configVo;
		}
		
		public function get energy():int {
			return _energy;
		}
		
	}

}