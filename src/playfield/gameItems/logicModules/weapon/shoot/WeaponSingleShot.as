package playfield.gameItems.logicModules.weapon.shoot {
	import configs.constants.GameObjectTypes;
	import configs.gameObjects.logics.weapon.shoot.WeaponSingleShotConfig;
	import misc.math.AngleUtil;
	import misc.math.MiscMath;
	import misc.vo.PositionAngleVo;
	import misc.vo.PositionVo;
	import playfield.events.EventCreateGameObject;
	import playfield.gameItems.WeaponGameObject;
	/**
	 * ...
	 * @author kk
	 */
	public class WeaponSingleShot implements IWeaponShoot {
		
		private var _owner:WeaponGameObject; // current Weapon Instance of this logic
		private var _singleShotConfig:WeaponSingleShotConfig; // this is the Logic config.
		
		//The owner of this Logic the weapon
		public function setOwner(weapon:WeaponGameObject):void  {
			_owner = weapon;
			_singleShotConfig = _owner.weaponShotConfig as WeaponSingleShotConfig;
		}
		
		//the weapon is fire
		public function fire():void {
			var calcAcc:Number;
			
			// get the postion of the weapon (x, y and rotation)
			var pos:PositionAngleVo = _owner.calcWeaponPos();
			
			// if the accuracy are set than calculate it
			if (_singleShotConfig.accuracy > 0) {
				calcAcc = MiscMath.randomNumber(0, _singleShotConfig.accuracy);
				if (MiscMath.randomNumber(0, 100) > 50) calcAcc *= -1;
				pos.rotation += AngleUtil.radFromDeg(calcAcc);
			}
			
			// create a new GameObject type GameObjectTypes.BULLET_TYPE, BulletType and create Position.
			_owner.createGameObject(GameObjectTypes.BULLET_TYPE, _owner.weaponConfig.bulletType, pos);
		}
		
		//execute on unload of the weapon, to clear some async-processes or reset some values.
		public function disable():void {
			//nothing todo...
		}
		
	}

}