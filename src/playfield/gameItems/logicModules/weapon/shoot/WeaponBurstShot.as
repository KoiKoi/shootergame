package playfield.gameItems.logicModules.weapon.shoot {
	import configs.constants.GameObjectTypes;
	import configs.gameObjects.logics.weapon.shoot.WeaponBurstShotConfig;
	import misc.math.AngleUtil;
	import misc.math.MiscMath;
	import misc.vo.PositionAngleVo;
	import misc.vo.PositionVo;
	import playfield.events.EventCreateGameObject;
	import playfield.gameItems.WeaponGameObject;
	/**
	 * ...
	 * @author kk
	 */
	public class WeaponBurstShot implements IWeaponShoot {
		
		private var _owner:WeaponGameObject;
		private var _burstShotConfig:WeaponBurstShotConfig;
		
		public function setOwner(weapon:WeaponGameObject):void  {
			_owner = weapon;
			_burstShotConfig = _owner.weaponShotConfig as WeaponBurstShotConfig;
		}
		
		public function disable():void {
			
		}
		
		public function fire():void {
			var pos:PositionAngleVo;
			var count:int = randomize(_burstShotConfig.amountMin, _burstShotConfig.amountMax);
			var dist:Number;
			
			for (var i:int = 0; i < count; i++) {
				pos = _owner.calcWeaponPos();
				pos.rotation += AngleUtil.radFromDeg(randomize(0, _burstShotConfig.spreading, true));
				dist =  randomize(0, _burstShotConfig.offsetBetweenShots, true);
				pos.x += dist * Math.cos(pos.rotation);
				pos.y += dist * Math.sin(pos.rotation);
				_owner.createGameObject(GameObjectTypes.BULLET_TYPE, _owner.weaponConfig.bulletType, pos);
			}
		}
		
		private function randomize(min:Number, max:Number, neg:Boolean = false):Number {
			var calc:Number;
			if (min == max) {
				return min
			}else {
				calc = MiscMath.randomNumber(min, max);
				if (neg) {
					if (MiscMath.randomNumber(0, 100) > 50) calc *= -1;
				}
				return calc;
			}
		}
		
		
		
	}

}