package playfield.gameItems.logicModules.weapon.shoot {
	import configs.constants.GameObjectTypes;
	import configs.gameObjects.logics.weapon.shoot.WeaponTribleShotConfig;
	import misc.math.AngleUtil;
	import misc.vo.PositionAngleVo;
	import playfield.gameItems.WeaponGameObject;
	/**
	 * ...
	 * @author kk
	 */
	public class WeaponTribleShot implements IWeaponShoot {
		
		private var _owner:WeaponGameObject;
		private var _anglePlus:Number;
		
		
		public function setOwner(weapon:WeaponGameObject):void  {
			_owner = weapon;
			_anglePlus = AngleUtil.radFromDeg(tribleShotConfig.spreading);
		}
		
		public function disable():void {
			
		}
		
		public function fire():void  {
			var shot1Pos:PositionAngleVo = _owner.calcWeaponPos();
			var shot2Pos:PositionAngleVo = _owner.calcWeaponPos();
			var shot3Pos:PositionAngleVo = _owner.calcWeaponPos();
			
			shot2Pos.rotation += _anglePlus;
			shot3Pos.rotation -= _anglePlus;
			
			_owner.createGameObject(GameObjectTypes.BULLET_TYPE, _owner.weaponConfig.bulletType, shot1Pos);
			_owner.createGameObject(GameObjectTypes.BULLET_TYPE, _owner.weaponConfig.bulletType, shot2Pos);
			_owner.createGameObject(GameObjectTypes.BULLET_TYPE, _owner.weaponConfig.bulletType, shot3Pos);
		}
		
		public function get tribleShotConfig():WeaponTribleShotConfig {
			return _owner.weaponShotConfig as WeaponTribleShotConfig
		}
		
		
	}

}