package playfield.gameItems.logicModules.weapon.shoot 
{
	import playfield.gameItems.WeaponGameObject;
	/**
	 * ...
	 * @author kk
	 */
	public interface IWeaponShoot {
		
		function setOwner(weapon:WeaponGameObject):void; // set owner
		function fire():void; // the fire Command of the weapon
		function disable():void; // calling when the weapon is unloading
		
	}

}