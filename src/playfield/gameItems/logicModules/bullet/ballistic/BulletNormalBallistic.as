package playfield.gameItems.logicModules.bullet.ballistic {
	import configs.constants.StarlingConst;
	import configs.gameObjects.vo.BulletConfigVo;
	import playfield.gameItems.BulletGameObject;
	import playfield.gameItems.WeaponGameObject;
	
	/**
	 * ...
	 * @author Der Korser
	 */
	public class BulletNormalBallistic implements IBulletBallistic {
		
		private var _owner:BulletGameObject;
		private var _bulletConfig:BulletConfigVo;
		
		public function setOwner(bullet:BulletGameObject):void {
			_owner = bullet;
			_bulletConfig = _owner.bulletConfig;
		}
		
		public function update():void {
			var speed:Number = _bulletConfig.speed;
			var calcAngle:Number = _owner.rotation;
			_owner.x += speed * Math.cos(calcAngle);
			_owner.y += speed * Math.sin(calcAngle);
		}
		
		public function disable():void {
			
		}
		
	}

}