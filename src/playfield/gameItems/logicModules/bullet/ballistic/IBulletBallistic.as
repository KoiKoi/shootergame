package playfield.gameItems.logicModules.bullet.ballistic 
{
	import playfield.gameItems.BulletGameObject;
	/**
	 * ...
	 * @author kk
	 */
	public interface IBulletBallistic {
		
		function setOwner(weapon:BulletGameObject):void;
		function update():void;
		function disable():void;
		
	}

}