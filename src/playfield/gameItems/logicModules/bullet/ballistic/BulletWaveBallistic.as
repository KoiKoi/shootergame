package playfield.gameItems.logicModules.bullet.ballistic {
	import configs.gameObjects.logics.bullet.ballistic.BulletSpinBallisticConfig;
	import configs.gameObjects.logics.bullet.ballistic.BulletWaveBallisticConfig;
	import misc.math.AngleUtil;
	import misc.math.MiscMath;
	import playfield.gameItems.BulletGameObject;
	/**
	 * ...
	 * @author Der Korser
	 */
	public class BulletWaveBallistic implements IBulletBallistic {
		
		private var _owner:BulletGameObject;
		private var _waveConfig:BulletWaveBallisticConfig;
		
		private var _frame:Number = 0;
		private var _side:Boolean;
		
		private var _dynFreq:Number;
		private var _dynSpin:Number;
		
		public function setOwner(bullet:BulletGameObject):void {
			_owner = bullet;
			_waveConfig = bullet.bulletBalisticConfig as BulletWaveBallisticConfig;
			_dynFreq = MiscMath.randomNumber(_waveConfig.frequenceFrom, _waveConfig.frequenceTo);
			_dynSpin = AngleUtil.radFromDeg(MiscMath.randomNumber(_waveConfig.spinFrom, _waveConfig.spinTo));
			_frame = MiscMath.randomNumber(0, _dynFreq);
			_side = (MiscMath.randomNumber(0, 100) > 50);
		}
		
		public function update():void {
			var speed:Number = _owner.bulletConfig.speed;
			var bulletAngle:Number = _owner.rotation;
			
			if (_frame <= (_dynFreq*-1)) {
				_side = false;
			}
			
			if (_frame >= _dynFreq) {
				_side = true;
			}
			
			if (_side) {
				_frame -= 1;
			}else {
				_frame += 1;
			}
			
			_owner.rotation -= (_dynSpin*_frame);
			_owner.x += speed * Math.cos(bulletAngle);
			_owner.y += speed * Math.sin(bulletAngle);
		}
		
		
		public function disable():void {
			_frame = MiscMath.randomNumber(0, _dynFreq);
			_side = (MiscMath.randomNumber(0, 100) > 50);
		}
		
	}

}