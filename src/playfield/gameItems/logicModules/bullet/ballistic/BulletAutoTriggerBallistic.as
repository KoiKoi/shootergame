package playfield.gameItems.logicModules.bullet.ballistic {
	import configs.gameObjects.logics.bullet.ballistic.BulletAutoTriggerBallisticConfig;
	import configs.gameObjects.logics.bullet.ballistic.BulletSpinBallisticConfig;
	import misc.math.MiscMath;
	import playfield.gameItems.BulletGameObject;
	/**
	 * ...
	 * @author Der Korser
	 */
	public class BulletAutoTriggerBallistic implements IBulletBallistic {
		
		private var _owner:BulletGameObject;
		private var _triggerConfig:BulletAutoTriggerBallisticConfig;
		private var _frameTime:int;
		private var _frame:Number = 0;
		private var _side:Boolean;
		
		public function setOwner(bullet:BulletGameObject):void {
			_owner = bullet;
			_frame = 0;
			_triggerConfig = (_owner.bulletBalisticConfig as BulletAutoTriggerBallisticConfig);
			_frameTime = MiscMath.randomNumber(_triggerConfig.triggerMinTime, _triggerConfig.triggerMaxTime);
		}
		
		public function update():void {
			
			
			var speed:Number = _owner.bulletConfig.speed;
			var calcAngle:Number = _owner.rotation;
			_owner.x += speed * Math.cos(calcAngle);
			_owner.y += speed * Math.sin(calcAngle);
			
			if (_frame >= _frameTime) {
				_owner.hitEnemy();
			}
			_frame++;
			
		}
		
		public function disable():void {
			_frame = 0;
		}
		
	}

}