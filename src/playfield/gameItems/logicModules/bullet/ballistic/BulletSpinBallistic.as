package playfield.gameItems.logicModules.bullet.ballistic {
	import configs.gameObjects.logics.bullet.ballistic.BulletSpinBallisticConfig;
	import misc.math.AngleUtil;
	import playfield.gameItems.BulletGameObject;
	/**
	 * ...
	 * @author Der Korser
	 */
	public class BulletSpinBallistic implements IBulletBallistic {
		
		private var _owner:BulletGameObject;
		private var _frame:Number = 0;
		private var _side:Boolean;
		private var _calcSpin:Number;
		private var _direction:Number;
		
		public function setOwner(bullet:BulletGameObject):void {
			_owner = bullet;
			_calcSpin = AngleUtil.radFromDeg(config.spin);
		}
		
		public function update():void {
			var speed:Number = _owner.bulletConfig.speed;
			var bulletAngle:Number = _owner.rotation;
			_owner.x += speed * Math.cos(bulletAngle);
			_owner.y += speed * Math.sin(bulletAngle);
			//_owner.mainGraphic.rotation += _calcSpin;
		}
		
		private function get config():BulletSpinBallisticConfig {
			return _owner.bulletBalisticConfig as BulletSpinBallisticConfig;
		}
		
		
		public function disable():void {
			//_owner.flatten();
			_frame = 0;
		}
		
	}

}