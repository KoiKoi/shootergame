package playfield.gameItems.logicModules.bullet.impact {
	import configs.gameObjects.vo.BulletConfigVo;
	import playfield.gameItems.BulletGameObject;
	/**
	 * ...
	 * @author Der Korser
	 */
	public class BulletNormalImpact implements IBulletImpact {
		
		private var _owner:BulletGameObject;
		private var _bulletConfig:BulletConfigVo;
		
		public function setOwner(weapon:BulletGameObject):void {
			_owner = weapon;
			_bulletConfig = weapon.bulletConfig;
		}
		
		public function hit():void {
			_owner.removeMe();
		}
		
		public function disable():void {
			
		}
		
	}

}