package playfield.gameItems.logicModules.bullet.impact {
	import configs.gameObjects.logics.bullet.impact.BulletDeflectImpactConfig;
	import configs.gameObjects.vo.BulletConfigVo;
	import misc.math.MiscMath;
	import playfield.gameItems.BulletGameObject;
	/**
	 * ...
	 * @author Der Korser
	 */
	public class BulletDeflectImpact implements IBulletImpact {
		
		private var _owner:BulletGameObject;
		private var _bulletConfig:BulletConfigVo;
		private var _defConfig:BulletDeflectImpactConfig;
		
		public function setOwner(bullet:BulletGameObject):void {
			_owner = bullet;
			_bulletConfig = bullet.bulletConfig;
			_defConfig = bullet.bulletImpactConfig as BulletDeflectImpactConfig;
		}
		
		public function hit():void {
			
			var random:Number = MiscMath.randomNumber(0, 100);
			var def:Number = MiscMath.randomNumber(_defConfig.deflectionMinDegrees, _defConfig.deflectionMaxDegrees);
			
			if (random > 50) {
				_owner.rotation += def;
			}else {
				_owner.rotation -= def;
			}
			
		}
		
		public function disable():void {
			
		}
	}

}