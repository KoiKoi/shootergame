package playfield.gameItems.logicModules.bullet.impact {
	import configs.constants.GameObjectTypes;
	import configs.gameObjects.logics.bullet.impact.BulletExplosionImpactConfig;
	import misc.math.MiscMath;
	import misc.vo.PositionAngleVo;
	import playfield.gameItems.BulletGameObject;
	
	/**
	 * ...
	 * @author Der Korser
	 */
	public class BulletExplosionImpact implements IBulletImpact {
		
		private var _explosionConf:BulletExplosionImpactConfig;
		private var _owner:BulletGameObject;
		
		private var _dymExpShrap:int;
		
		public function setOwner(weapon:BulletGameObject):void {
			_owner = weapon;
			_explosionConf = weapon.bulletImpactConfig as BulletExplosionImpactConfig;
			_dymExpShrap = MiscMath.randomNumber(_explosionConf.shrapnelMinAmount, _explosionConf.shrapnelMaxAmount);
		}
		
		public function hit():void {
			var posAng:PositionAngleVo;
			for (var i:int = 0; i < _dymExpShrap; i++) {
				posAng = new PositionAngleVo(_owner.x, _owner.y, MiscMath.randomNumber(0, 359));
				_owner.createGameObject(GameObjectTypes.BULLET_TYPE, _explosionConf.shrapnelBulletType, posAng);
			}
			_owner.removeMe();
		}
		
		public function disable():void {
		}
	}

}