package playfield.gameItems.logicModules.bullet.impact 
{
	import playfield.gameItems.BulletGameObject;
	
	/**
	 * ...
	 * @author kk
	 */
	public interface IBulletImpact {
		
		function setOwner(weapon:BulletGameObject):void;
		function hit():void;
		function disable():void;
		
	}

}