package playfield.gameItems.logicModules.bullet.impact {
	import configs.gameObjects.logics.bullet.impact.BulletPenetrationImpactConfig;
	import misc.math.MiscMath;
	import playfield.gameItems.BulletGameObject;
	/**
	 * ...
	 * @author Der Korser
	 */
	public class BulletPenetrationImpact implements IBulletImpact {
		
		private var _owner:BulletGameObject;
		private var _config:BulletPenetrationImpactConfig;
		
		public function setOwner(weapon:BulletGameObject):void {
			_owner = weapon;
			_config = weapon.bulletImpactConfig as BulletPenetrationImpactConfig;
		}
		
		public function hit():void {
			if (MiscMath.randomNumber(0, 100) < _config.percentStick) {
				_owner.removeMe();
			}
		}
		
		public function disable():void {
			
		}
		
	}

}