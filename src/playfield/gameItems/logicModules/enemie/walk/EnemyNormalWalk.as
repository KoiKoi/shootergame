package playfield.gameItems.logicModules.enemie.walk {
	import configs.constants.StarlingConst;
	import configs.gameObjects.vo.EnemyConfigVo;
	import misc.math.AngleUtil;
	import misc.math.MiscMath;
	import misc.vo.PositionAngleVo;
	import playfield.gameItems.EnemyGameObject;
	
	/**
	 * ...
	 * @author kk
	 */
	public class EnemyNormalWalk implements IEnemyWalk {
		
		private var _owner:EnemyGameObject;
		private var _configVo:EnemyConfigVo;
		private var _calcSpeed:Number;
		private var _calcX:Number;
		private var _calcY:Number;
		
		public function setOwner(enemy:EnemyGameObject):void {
			_owner = enemy;
			_configVo = enemy.configVo;
			_calcSpeed = MiscMath.randomNumber(_configVo.minSpeed, _configVo.maxSpeed);
			_calcX = (MiscMath.randomNumber(-10, 10));
			_calcY = (MiscMath.randomNumber(-10, 10));
		}
		
		public function update(target:PositionAngleVo):void {
			var x:int = target.x + _calcX;
			var y:int = target.y + _calcY;
			
			_owner.rotation = MiscMath.getAngle(_owner.x, _owner.y, x, y,-StarlingConst.STARLING_90_DEG);
			var calcAngle:Number = _owner.rotation - StarlingConst.STARLING_90_DEG;
			_owner.x += _calcSpeed * Math.cos(calcAngle);
			_owner.y += _calcSpeed * Math.sin(calcAngle);
		}
		
		public function disable():void {
			
		}
		
		
		
	}

}