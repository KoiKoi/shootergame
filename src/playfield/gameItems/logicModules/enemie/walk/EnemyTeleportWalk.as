package playfield.gameItems.logicModules.enemie.walk {
	import configs.constants.StarlingConst;
	import configs.gameObjects.logics.enemie.walk.EnemyJumpWalkConfig;
	import configs.gameObjects.logics.enemie.walk.EnemyTeleportWalkConfig;
	import configs.gameObjects.vo.EnemyConfigVo;
	import misc.math.AngleUtil;
	import misc.math.MiscMath;
	import misc.vo.PositionAngleVo;
	import playfield.gameItems.EnemyGameObject;
	
	/**
	 * ...
	 * @author kk
	 */
	public class EnemyTeleportWalk implements IEnemyWalk {
		
		private static const ALPHA_EFFECT_TIME:int = 1;
		
		private static const TELEPORT_STATE_NONE:int = 0;
		private static const TELEPORT_STATE_START:int = 1;
		private static const TELEPORT_STATE_SWITCH:int = 2;
		private static const TELEPORT_STATE_END:int = 3;
		
		private var _owner:EnemyGameObject;
		private var _configVo:EnemyConfigVo;
		private var _teleportConfigVo:EnemyTeleportWalkConfig;
		
		private var _calcSpeed:Number;
		private var _calcDistance:Number;
		private var _calcRotation:Number;
		private var _calcFrameTime:Number;
		
		private var _currentFrame:Number;
		private var _teleportMode:int;
		
		
		public function setOwner(enemy:EnemyGameObject):void {
			_owner = enemy;
			_configVo = enemy.configVo;
			_currentFrame = 0;
			_teleportConfigVo = enemy.walkLogicConfig as EnemyTeleportWalkConfig;
			_teleportMode = TELEPORT_STATE_NONE;
			refreshValues();
		}
		
		
		public function update(target:PositionAngleVo):void {
			var calcAngle:Number;
			_currentFrame++;
			if (_teleportMode == TELEPORT_STATE_NONE) {
				
				_owner.rotation = MiscMath.getAngle(_owner.x, _owner.y, target.x, target.y,-StarlingConst.STARLING_90_DEG);
				calcAngle = _owner.rotation - StarlingConst.STARLING_90_DEG;
				_owner.x += _calcSpeed * Math.cos(calcAngle);
				_owner.y += _calcSpeed * Math.sin(calcAngle);
				
				
				if (_currentFrame >= _calcFrameTime) {
					_owner.rotation += AngleUtil.radFromDeg(_calcRotation);
					_teleportMode = TELEPORT_STATE_START;
					_currentFrame = 0;
				}
			}else {
				if (_teleportMode == TELEPORT_STATE_START) {
					_owner.alpha -= ((_currentFrame * ALPHA_EFFECT_TIME)/100);
					if (_owner.alpha < 0.2) {
						_teleportMode = TELEPORT_STATE_SWITCH;
						_currentFrame = 0;
					}
				}else if (_teleportMode == TELEPORT_STATE_SWITCH) {
					
					calcAngle = _owner.rotation - StarlingConst.STARLING_90_DEG;
					_owner.x += _calcDistance * Math.cos(calcAngle);
					_owner.y += _calcDistance * Math.sin(calcAngle);
					
					_teleportMode = TELEPORT_STATE_END;
					_currentFrame = 0;
					
				}else if (_teleportMode == TELEPORT_STATE_END) {
					_owner.alpha += ((_currentFrame * ALPHA_EFFECT_TIME)/100);
					if (_owner.alpha >= 1) {
						_teleportMode = TELEPORT_STATE_NONE;
						_currentFrame = 0;
						refreshValues();
					}
				}
			}
		}
		
		private function refreshValues():void {
			_calcSpeed = MiscMath.randomNumber(_configVo.minSpeed, _configVo.maxSpeed);
			_calcDistance = MiscMath.randomNumber(_teleportConfigVo.minDistance, _teleportConfigVo.maxDistance);
			_calcRotation = MiscMath.randomNumber(25, 120);
			_calcFrameTime = MiscMath.randomNumber(_teleportConfigVo.delayMinTime, _teleportConfigVo.delayMinTime);
			
			if (MiscMath.randomNumber(0, 100) > 50)_calcDistance *= -1;
			if (MiscMath.randomNumber(0, 100) > 50)_calcRotation *= -1;
		}
		
		
		public function disable():void {
			refreshValues();
			_currentFrame = 0;
			_teleportMode = TELEPORT_STATE_NONE;
			_owner.alpha = 100;
		}
		
		
		
	}

}