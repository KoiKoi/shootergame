package playfield.gameItems.logicModules.enemie.walk {
	import configs.constants.StarlingConst;
	import configs.gameObjects.logics.enemie.walk.EnemyJumpWalkConfig;
	import configs.gameObjects.vo.EnemyConfigVo;
	import misc.math.MiscMath;
	import misc.vo.PositionAngleVo;
	import playfield.gameItems.EnemyGameObject;
	
	/**
	 * ...
	 * @author kk
	 */
	public class EnemyJumpWalk implements IEnemyWalk {
		
		private var _owner:EnemyGameObject;
		private var _jumpConfig:EnemyJumpWalkConfig;
		private var _configVo:EnemyConfigVo;
		
		
		private var _currentFrame:int;
		private var _jumping:Boolean;
		private var _delay:int;
		private var _calcSpeed:Number;
		
		public function setOwner(enemy:EnemyGameObject):void {
			_owner = enemy;
			_jumpConfig = (enemy.walkLogicConfig as EnemyJumpWalkConfig);
			_configVo = enemy.configVo;
			_delay = MiscMath.randomNumber(_jumpConfig.delayMinTime, _jumpConfig.delayMaxTime);
			_calcSpeed = MiscMath.randomNumber(_configVo.minSpeed, _configVo.maxSpeed);
			_currentFrame = MiscMath.randomNumber(0, _jumpConfig.delayMaxTime);
		}
		
		public function update(target:PositionAngleVo):void {
			var calcAngle:Number;
			_currentFrame++;
			if (_jumping) {
				
				calcAngle = _owner.rotation - StarlingConst.STARLING_90_DEG;
				_owner.x += _calcSpeed * Math.cos(calcAngle);
				_owner.y += _calcSpeed * Math.sin(calcAngle);
				
				
				if (_delay <= _currentFrame) {
					_jumping = false;
					_currentFrame = 0;
				}
			}else {
				_owner.rotation = MiscMath.getAngle(_owner.x, _owner.y, target.x, target.y,-StarlingConst.STARLING_90_DEG);
				//_owner.rotation = MiscMath.fAngle(_owner.x, _owner.y, target.x, target.y) + 90;
				
				if (_delay <= _currentFrame) {
					_jumping = true;
					_currentFrame = 0;
				}
			}
		}
		
		public function disable():void {
			_currentFrame = 0;
		}
		
		
		
	}

}