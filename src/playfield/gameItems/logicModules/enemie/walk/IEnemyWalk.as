package playfield.gameItems.logicModules.enemie.walk {
	import misc.vo.PositionAngleVo;
	import playfield.gameItems.EnemyGameObject;
	/**
	 * ...
	 * @author kk
	 */
	public interface IEnemyWalk {
		
		function setOwner(weapon:EnemyGameObject):void;
		function update(target:PositionAngleVo):void;
		function disable():void;
		
	}

}