package playfield.gameItems.logicModules.enemie.decease {
	import configs.constants.GameObjectTypes;
	import configs.gameObjects.logics.enemie.decease.EnemyExplodeDeceaseConfig;
	import misc.math.AngleUtil;
	import misc.math.MiscMath;
	import misc.vo.PositionAngleVo;
	import playfield.gameItems.EnemyGameObject;
	
	/**
	 * ...
	 * @author kk
	 */
	public class EnemyExplodeDecease implements IEnemyDecease {
		
		private var _owner:EnemyGameObject;
		private var _decConfig:EnemyExplodeDeceaseConfig;
		
		public function setOwner(enemy:EnemyGameObject):void {
			_owner = enemy;
			_decConfig = enemy.deceaseLogicConfig as EnemyExplodeDeceaseConfig;
		}
		
		public function decease():void {
			var pos:PositionAngleVo;
			var xPos:Number 
			var yPos:Number 
			var count:int = Math.round(MiscMath.randomNumber(_decConfig.minAmount, _decConfig.maxAmount));
			
			for (var i:int = 0; i < count; i++) {
				xPos = MiscMath.randomNumber(0, _decConfig.radius);
				yPos = MiscMath.randomNumber(0, _decConfig.radius);
				
				if ((MiscMath.randomNumber(0, 100)) > 50) xPos *= -1;
				if ((MiscMath.randomNumber(0, 100)) > 50) yPos *= -1;
				
				pos = new PositionAngleVo(xPos + _owner.x, yPos + _owner.y, AngleUtil.radFromDeg(MiscMath.randomNumber(0, 359)));
				_owner.createGameObject(GameObjectTypes.ENEMY_TYPE, _decConfig.spawnEnemieType, pos);
			}
		}
		
		public function disable():void {
			
		}
		
		
		
	}

}