package playfield.gameItems.logicModules.enemie.decease {
	import playfield.gameItems.EnemyGameObject;
	/**
	 * ...
	 * @author Der Korser
	 */
	public interface IEnemyDecease {
		function setOwner(enemy:EnemyGameObject):void;
		function decease():void
		function disable():void;
	}

}