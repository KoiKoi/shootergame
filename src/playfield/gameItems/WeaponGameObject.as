package playfield.gameItems {
	
	import com.greensock.TweenLite;
	import configs.constants.GameObjectTypes;
	import configs.constants.StarlingConst;
	import configs.gameObjects.logics.weapon.shoot.MainWeaponShotConfig;
	import configs.gameObjects.module.WeaponSetupModule;
	import configs.gameObjects.vo.WeaponConfigVo;
	import misc.math.AngleUtil;
	import starling.display.DisplayObject;
	import misc.vo.PositionAngleVo;
	import org.osflash.signals.Signal;
	import playfield.gameItems.logicModules.weapon.shoot.IWeaponShoot;
	/**
	 * ...
	 * @author Der Korser
	 */
	public class WeaponGameObject extends MainGameObject {
		
		private var _weaponSetupModule	:WeaponSetupModule;
		private var _shotLogic			:IWeaponShoot;
		private var _owner				:HeroGameObject;
		
		private var _ammo				:Number;
		private var _unlimitAmmo		:Boolean;
		private var _fireDelay			:TweenLite;
		private var _isPressFire		:Boolean;
		private var _readyToFire		:Boolean;
		
		public function WeaponGameObject(module:WeaponSetupModule, graphic:DisplayObject, signal:Signal) {
			super(signal, GameObjectTypes.WEAPON_TYPE, module.configVo.type);
			_weaponSetupModule = module;
			changeMainGraphic(graphic);
			init();
		}
		
		private function init():void {
			initLogic();
			reInit();
		}
		
		private function reFillAmmo():void {
			_unlimitAmmo = (weaponConfig.maxAmmo == -1);
			_ammo = weaponConfig.maxAmmo;
		}
		
		private function initLogic():void {
			var logClass:Class = (_weaponSetupModule.weaponShotLogic.logicClass as Class)
			_shotLogic = new logClass();
			_shotLogic.setOwner(this);
		}
		
		override public function reInit():void {
			super.reInit();
			killFireDelay();
			reFillAmmo();
			_shotLogic.disable();
			_readyToFire = true;
			_isPressFire = false;
			_owner = null;
		}
		
		
		public function setOwner(hero:HeroGameObject):void {
			killFireDelay();
			_isPressFire = false;
			_owner = hero;
		}
		
		public function fire(value:Boolean):void {
			if (_owner) {
				_isPressFire = value;
				executeFire();
			}
		}
		
		public function executeFire():void {
			if (_owner) {
				if (_readyToFire && ((_ammo > 0) || _unlimitAmmo) && _isPressFire) {
					_readyToFire = false;
					_shotLogic.fire();
					_ammo--;
					_fireDelay = TweenLite.delayedCall(weaponConfig.delay, onWeaponReady);
				}
			}
		}
		
		
		public function calcWeaponPos():PositionAngleVo {
			var result:PositionAngleVo = new PositionAngleVo();
			
			if (_owner) {
				var playerX:Number = (owner.x);
				var PlayerY:Number = (owner.y);
				
				var weaponX:Number = (this.x);
				var weaponY:Number = (this.y);
				
				
				var playerRadians:Number = owner.rotation - StarlingConst.STARLING_90_DEG;
				var cos:Number = Math.cos(playerRadians);
				var sin:Number = Math.sin(playerRadians);
				
				var xWeaponCos:Number = ((weaponY * cos));
				var yWeaponCos:Number = ((weaponY * sin));
				
				var calcCos:Number = Math.cos(playerRadians + StarlingConst.STARLING_90_DEG);
				var calcSin:Number = Math.sin(playerRadians + StarlingConst.STARLING_90_DEG);
				
				var xResult:Number = ((weaponX * calcCos) + xWeaponCos);
				var yResult:Number = ((weaponX * calcSin) + yWeaponCos);
				
				result.x = xResult + owner.x;
				result.y = yResult + owner.y;
				result.rotation = playerRadians;
			}
			
			return result;
			
		}
		
		private function onWeaponReady():void {
			killFireDelay();
			_readyToFire = true;
			if (_isPressFire) {
				executeFire();
			}
		}
		
		private function killFireDelay():void {
			if (_fireDelay) {
				_fireDelay.kill();
				_fireDelay = null;
			}
		}
		
		override public function deactivate():void {
			super.deactivate();
			killFireDelay();
			_shotLogic.disable();
		}
		
		
		public function calcPositionAngle():PositionAngleVo {
			var position:PositionAngleVo = new PositionAngleVo(this.x, this.y, this.rotation);
			if (owner) {
				position.x += owner.x;
				position.y += owner.y;
				position.rotation += owner.rotation;
			}
			return position;
		}
		
		// Getter And Setter Configs
		
		public function get weaponShotConfig():MainWeaponShotConfig {
			return _weaponSetupModule.weaponShotLogic;
		}
		
		public function get weaponConfig():WeaponConfigVo {
			return _weaponSetupModule.configVo;
		}
		
		public function get weaponSetupModule():WeaponSetupModule {
			return _weaponSetupModule;
		}
		
		public function get hasOwner():Boolean {
			return (_owner != null);
		}
		
		public function get owner():HeroGameObject {
			return _owner;
		}
		
		public function get isPressFire():Boolean {
			return _isPressFire;
		}
		
	}

}