package playfield.gameItems 
{
	import com.greensock.TweenLite;
	import configs.constants.GameObjectTypes;
	import configs.gameObjects.logics.bullet.ballistic.MainBulletBalisticConfig;
	import configs.gameObjects.logics.bullet.impact.MainBulletImpactConfig;
	import configs.gameObjects.module.BulletSetupModule;
	import configs.gameObjects.vo.BulletConfigVo;
	import starling.display.DisplayObject;
	import org.osflash.signals.Signal;
	import playfield.gameItems.logicModules.bullet.ballistic.IBulletBallistic;
	import playfield.gameItems.logicModules.bullet.impact.IBulletImpact;
	/**
	 * ...
	 * @author kk
	 */
	public class BulletGameObject extends MainGameObject {
		
		private var _bulletSetupModule	:BulletSetupModule;
		private var _ballisticLogic		:IBulletBallistic;
		private var _impactLogic		:IBulletImpact;
		private var _lifeTime			:TweenLite;
		
		public function BulletGameObject(setupModule:BulletSetupModule, graphic:DisplayObject, signal:Signal) {
			super(signal, GameObjectTypes.BULLET_TYPE, setupModule.configVo.type);
			_bulletSetupModule = setupModule;
			changeMainGraphic(graphic);
			initLogic();
			startLifeTime();
		}
		
		private function initLogic():void {
			var balLogClass:Class = (_bulletSetupModule.balisticLogicConfig.logicClass as Class)
			_ballisticLogic = new balLogClass();
			_ballisticLogic.setOwner(this);
			
			var impactClass:Class = (_bulletSetupModule.impactLogicConfig.logicClass as Class)
			_impactLogic = new impactClass();
			_impactLogic.setOwner(this);
		}
		
		
		
		override public function update():void {
			_ballisticLogic.update();
		}
		
		public function hitEnemy():void {
			_impactLogic.hit();
		}
		
		override public function reInit():void {
			_impactLogic.disable();
			_ballisticLogic.disable();
			startLifeTime();
		}
		
		override public function deactivate():void {
			super.deactivate();
			_impactLogic.disable();
			_ballisticLogic.disable();
			killLifeTime();
		}
		
		
		private function killLifeTime():void {
			if (_lifeTime) {
				_lifeTime.kill();
			}
		}
		
		private function startLifeTime():void {
			killLifeTime();
			_lifeTime = TweenLite.delayedCall(bulletConfig.lifeTime, removeMe);
		}
		
		// Getter And Setter Configs
		public function get bulletImpactConfig():MainBulletImpactConfig {
			return _bulletSetupModule.impactLogicConfig;
		}
		
		public function get bulletBalisticConfig():MainBulletBalisticConfig {
			return _bulletSetupModule.balisticLogicConfig;
		}
		
		public function get bulletConfig():BulletConfigVo {
			return _bulletSetupModule.configVo;
		}
		
		public function get weaponSetupModule():BulletSetupModule {
			return _bulletSetupModule;
		}
		
	}

}