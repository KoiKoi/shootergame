package playfield.gameItems {
	import configs.constants.GameObjectTypes;
	import configs.gameObjects.logics.enemie.decease.MainEnemyDeceaseConfig;
	import configs.gameObjects.logics.enemie.walk.MainEnemyWalkConfig;
	import configs.gameObjects.module.EnemySetupModule;
	import configs.gameObjects.vo.EnemyConfigVo;
	import starling.display.DisplayObject;
	import misc.vo.PositionAngleVo;
	import org.osflash.signals.Signal;
	import playfield.gameItems.logicModules.enemie.decease.IEnemyDecease;
	import playfield.gameItems.logicModules.enemie.walk.IEnemyWalk;
	/**
	 * ...
	 * @author kk
	 */
	public class EnemyGameObject extends MainGameObject {
		
		private var _enemySetupModule	:EnemySetupModule;
		private var _deceaseLogic		:IEnemyDecease;
		private var _walkLogic			:IEnemyWalk;
		
		private var _targetPosition		:PositionAngleVo;
		
		private var _energy				:Number;
		
		
		public function EnemyGameObject(setupModule:EnemySetupModule, graphic:DisplayObject, signal:Signal) {
			super(signal, GameObjectTypes.ENEMY_TYPE, setupModule.configVo.type);
			_enemySetupModule = setupModule;
			changeMainGraphic(graphic);
			initLogic();
			_energy = configVo.maxEnergy;
		}
		
		private function initLogic():void {
			var decLogClass:Class = (_enemySetupModule.deceaseLogicConfig.logicClass as Class)
			_deceaseLogic = new decLogClass();
			_deceaseLogic.setOwner(this);
			
			var walkClass:Class = (_enemySetupModule.walkLogicConfig.logicClass as Class)
			_walkLogic = new walkClass();
			_walkLogic.setOwner(this);
		}
		
		override public function reInit():void {
			super.reInit();
			_energy = configVo.maxEnergy;
			_walkLogic.disable();
			_deceaseLogic.disable();
		}
		
		public function updateTargetPos(target:PositionAngleVo):void {
			_targetPosition = target;
		}
		
		override public function update():void {
			if (isAlive) {
				_walkLogic.update(_targetPosition);
			}
		}
		
		override public function deactivate():void {
			super.deactivate();
			_walkLogic.disable();
			_deceaseLogic.disable();
		}
		
		public function damage(value:Number):void {
			_energy -= value;
			if ((_energy <= 0) && (isAlive)) {
				_deceaseLogic.decease();
				removeMe();
			}
		}
		
		// Getter And Setter Configs
		public function get enemySetupModule():EnemySetupModule {
			return _enemySetupModule;
		}
		
		public function get hitPoints():int {
			return _enemySetupModule.configVo.hitPoints;
		}
		
		public function get configVo():EnemyConfigVo {
			return _enemySetupModule.configVo;
		}
		
		public function get deceaseLogicConfig():MainEnemyDeceaseConfig {
			return _enemySetupModule.deceaseLogicConfig;
		}
		
		public function get walkLogicConfig():MainEnemyWalkConfig {
			return _enemySetupModule.walkLogicConfig;
		}
	}

}