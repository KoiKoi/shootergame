package playfield.gameItems {
	import configs.constants.StarlingConst;
	import flash.geom.Point;
	import misc.math.AngleUtil;
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import misc.vo.PositionAngleVo;
	import org.osflash.signals.Signal;
	import playfield.events.EventCreateGameObject;
	import playfield.events.EventInfoGameObject;
	import playfield.events.EventRemoveGameObject;
	
	/**
	 * ...
	 * @author Der Korser
	 */
	public class MainGameObject {
		
		private var _mainGraphic		:DisplayObject;
		private var _playfieldSignal	:Signal;
		private var _gameObjectType		:int;
		protected var _subType			:int;
		protected var _id				:Number;
		private var _isAlive			:Boolean;
		private var _point				:Point;
		private var _hitRange			:Number;
		private var _posAngleVo			:PositionAngleVo;
		
		
		public function MainGameObject(signal:Signal, objectType:int, sType:int):void {
			_id = -1;
			_gameObjectType = objectType;
			_subType = sType;
			_playfieldSignal = signal;
			
			_isAlive = true;
			
			_posAngleVo = new PositionAngleVo();
		}
		
		public function changeMainGraphic(asset:DisplayObject):void {
			if (asset) {
				_mainGraphic = asset;
				if (_mainGraphic.width > _mainGraphic.height) {
					_hitRange = (_mainGraphic.height * 0.5);
				}else {
					_hitRange = (_mainGraphic.width * 0.5);
				}
				
			}
		}
		
		public function reInit():void {
			_isAlive = true;
		}
		
		public function update():void {
			//Must be override
		}
		
		public function hit():void {
			//Must be override
		}
		
		public function deactivate():void {
			_isAlive = false;
		}
		
		public function removeMe():void {
			deactivate();
			removeGameObject(_id, _gameObjectType);
		}
		
		
		public function removeGameObject(objectId:int,objectType:int):void {
			var event:EventRemoveGameObject = new EventRemoveGameObject();
			event.remitterId = _id;
			event.targetId = objectId;
			event.targetType = objectType;
			_playfieldSignal.dispatch(event);
		}
		
		public function createGameObject(objectType:int, subType:int, PosAngle:PositionAngleVo):void {
			var event:EventCreateGameObject = new EventCreateGameObject();
			event.subType = subType;
			event.objectType = objectType;
			event.remitterId = _id;
			event.createPosAngle = PosAngle;
			_playfieldSignal.dispatch(event);
		}
		
		public function infoGameObject(health:int):void {
			var event:EventInfoGameObject = new EventInfoGameObject();
			event.health = health;
			event.gameObjectId = _id;
			event.gameObjectType = _gameObjectType;
			_playfieldSignal.dispatch(event);
		}
		
		public function setPositionAngle(value:PositionAngleVo):void {
			this.x = value.x;
			this.y = value.y;
			this.rotation = value.rotation;
		}
		
		//Getter / Setter
		public function get positionAngleVo():PositionAngleVo {
			return _posAngleVo;
		}
		
		public function get gameObjectType():int {
			return _gameObjectType;
		}
		
		public function get subType():int {
			return _subType;
		}
		
		public function get id():Number {
			return _id;
		}
		
		public function set id(value:Number):void {
			_id = value;
		}		
		
		public function get y():Number {
			return _posAngleVo.y;
		}
		
		public function set y(value:Number):void {
			if (_mainGraphic) _mainGraphic.y = value;
			_posAngleVo.y = value;
		}		
		
		public function get x():Number {
			return _posAngleVo.x;
		}
		
		public function set x(value:Number):void {
			if (_mainGraphic) _mainGraphic.x = value;
			_posAngleVo.x = value;
		}
		
		public function set alpha(value:Number):void {
			if (_mainGraphic) _mainGraphic.alpha = value;
		}
		
		public function get alpha():Number {
			if (_mainGraphic) return _mainGraphic.alpha;
			return -1;
		}		
		
		public function set visible(value:Boolean):void {
			if (_mainGraphic) _mainGraphic.visible = value;
		}
		
		public function get visible():Boolean {
			if (_mainGraphic) return _mainGraphic.visible;
			return false;
		}
		
		
		public function get rotation():Number {
			return _posAngleVo.rotation;
		}
		
		public function set rotation(value:Number):void {
			if (_mainGraphic) _mainGraphic.rotation = value;
			_posAngleVo.rotation = value;
		}		
		
		public function get mainGraphic():DisplayObject {
			return _mainGraphic;
		}
		
		public function get isAlive():Boolean {
			return _isAlive;
		}
		
		public function get point():Point {
			return (_posAngleVo as Point);
		}
		
		public function get hitRange():Number {
			return _hitRange;
		}
		
		
	}

}