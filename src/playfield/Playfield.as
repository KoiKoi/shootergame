package playfield 
{
	import com.greensock.TweenLite;
	import configs.constants.GameObjectTypes;
	import configs.MainConfig;
	import configs.gameObjects.constants.EnemyTypes;
	import configs.gameObjects.constants.WeaponTypes;
	import configs.gameObjects.module.BulletSetupModule;
	import configs.gameObjects.module.WeaponSetupModule;
	import flash.utils.Dictionary;
	import logic.input.state.GamePadState;
	import logic.input.state.TouchState;
	import misc.controller.constants.ControlButtonsConst;
	import playfield.gameItems.MainGameObject;
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import graphics.GraphicLibrary;
	import logic.input.KeyboardInput;
	import logic.input.state.KeyState;
	import logic.input.MouseInput;
	import logic.input.state.MouseState;
	import misc.math.MiscMath;
	import misc.vo.PositionAngleVo;
	import org.osflash.signals.Signal;
	import playfield.events.EventCreateGameObject;
	import playfield.events.EventGameObject;
	import playfield.events.EventInfoGameObject;
	import playfield.events.EventRemoveGameObject;
	import playfield.gameItems.BulletGameObject;
	import playfield.gameItems.EnemyGameObject;
	import playfield.gameItems.HeroGameObject;
	import playfield.gameItems.WeaponGameObject;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	/**
	 * ...
	 * @author kk
	 */
	public class Playfield extends Sprite {
		
		//Input Signals
		private var _mouseSignal:Signal;
		private var _touchSignal:Signal;
		private var _keyboardSignal:Signal;
		private var _gamePadSignal:Signal;
		
		//Playfield
		private var _playfieldSignal:Signal;
		
		private var _gameObjectManager:GameObjectManager;
		
		private var _player:HeroGameObject;
		private var _objectPool:GameObjectPool;
		
		private var _spawmEnemieTimer:TweenLite;
		private var _spawmWeaponTimer:TweenLite;
		
		private var _background:DisplayObject;
		
		private var _playFieldArea:Sprite;
		private var _playFieldLayers:Dictionary; // Of sprites..
		
		private var _hud:PlayfieldHud;
		
		private var _round:int;
		
		public function Playfield(config:MainConfig, inputKeyboardSignal:Signal, inputMouseSignal:Signal, inputTouchSignal:Signal, inputGamePadSignal:Signal) {
			
			//register Signals
			_mouseSignal = inputMouseSignal;
			_keyboardSignal = inputKeyboardSignal;
			_touchSignal = inputTouchSignal;
			_gamePadSignal = inputGamePadSignal;
			
			// Init the Main-Configs
			init(config);
			
			//Start the Game
			start();
		}
		
		private function init(config:MainConfig):void {
			
			//Init Gamearea
			_playFieldLayers = new Dictionary();
			_playFieldArea = new Sprite();
			
			addPlayFieldLayer(GameObjectTypes.HERO_TYPE);
			addPlayFieldLayer(GameObjectTypes.WEAPON_TYPE);
			addPlayFieldLayer(GameObjectTypes.ENEMY_TYPE);
			addPlayFieldLayer(GameObjectTypes.BULLET_TYPE);
			
			//Player Hud
			_hud = new PlayfieldHud();
			_round = 0;
			
			//Add Game View-Stages
			addChild(_playFieldArea);
			addChild(_hud);
			
			//Init Signals
			_playfieldSignal = new Signal();
			_mouseSignal.add(interactMouse);
			_keyboardSignal.add(interactKeyBoard);
			_playfieldSignal.add(interactPlayfieldSignal);
			_touchSignal.add(interactTouch);
			_gamePadSignal.add(interactGamepad);
			
			//Init Instanzes
			_gameObjectManager = new GameObjectManager(config, _playfieldSignal);
			
			//Background init
			switchBackGround(graphicLib.getGraphic(GraphicLibrary.BG_GRAPHIC) as DisplayObject);
		}
		

		
		public function getPlayfieldLayersByType(type:int):Sprite {
			return _playFieldLayers[type];
		}
		
		private function addPlayFieldLayer(areaId:int):void {
			var area:Sprite = new Sprite();
			_playFieldArea.addChild(area);
			_playFieldLayers[areaId] = area;
		}
		
		private function addChildToPlayFieldLayer(gameObject:MainGameObject):void {
			if (_playFieldLayers[gameObject.gameObjectType] != null) {
				(_playFieldLayers[gameObject.gameObjectType] as Sprite).addChild(gameObject.mainGraphic);
			}
		}
		
		private function removeChildToPlayFieldLayer(gameObject:MainGameObject):void {
			if ((gameObject) && (_playFieldLayers[gameObject.gameObjectType] != null)) {
				var layer:Sprite = (_playFieldLayers[gameObject.gameObjectType] as Sprite);
				if (layer.contains(gameObject.mainGraphic)) {
					layer.removeChild(gameObject.mainGraphic);
				}
			}
		}
		
		//Add the Background
		private function switchBackGround(bgDisplayObj:DisplayObject):void {
			
			//bgDisplayObj.alpha = 0.2;
			
			if (_background) {
				if (_playFieldArea.contains(_background)) {
					_playFieldArea.removeChild(_background);
				}
			}
			bgDisplayObj.pivotX = 0;
			bgDisplayObj.pivotY = 0;
			bgDisplayObj.x = 0;
			bgDisplayObj.y = 0;
			_playFieldArea.addChildAt(bgDisplayObj, 0);
			_background = bgDisplayObj;
		}
		
		
		private function start():void {
			
			//Create the Hero
			createHero();
			
			//Spawn the Weapons
			letsSpawnWeapons();
			
			//Spwan the Enemies
			letsSpawnEnemies();
			
			//Spawn Hero
			spawnHero();
			
			//On Enter_Frame tick
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		
		// Here is Game Over, Game Stops
		private function gameOver():void {
			this.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			
			if (_spawmEnemieTimer) {
				_spawmEnemieTimer.kill();
			}
			if (_spawmWeaponTimer) {
				_spawmWeaponTimer.kill();
			}
			_hud.gameOver();
		}
		
		private function spawnHero():void {
			if ((_player) && (_player.isAlive)) {
				addChildToPlayFieldLayer(_player);
			}
		}
		
		//Create the Main Hero
		private function createHero():void {
			removeChildToPlayFieldLayer(_player);
			_player = _gameObjectManager.createHero();
			_hud.currentHealth = _player.energy;
		}
		
		//Spawn Enemies, this function will be delayed call every 10 secounds (Next Round)
		private function letsSpawnEnemies():void {
			
			//Every Round will be harder..
			spawnCreaps(EnemyTypes.PORTER_ENEMIE, 1 + (1.5 * _round), 800, 1500);
			spawnCreaps(EnemyTypes.JUMPER_ENEMIE, 2 + (1.5 * _round), 800, 1500);
			spawnCreaps(EnemyTypes.CRIC_ENEMIE, 15 + (1.5 * _round), 800, 1250);
			
			_spawmEnemieTimer = TweenLite.delayedCall(10, letsSpawnEnemies);
			
			//Next round begins
			_round++;
			_hud.currentRound = _round;
		}
		
		//Spawn Weapons, this function will be delayed call every 20 secounds.
		private function letsSpawnWeapons():void {
			
			//Round 0 all Weapons spawn, nextround a random weapon spawn all 20 sec
			if (_round == 0) {
				spawnWeapon(WeaponTypes.WEAPON_WAVE, 80, 350);
				spawnWeapon(WeaponTypes.WEAPON_GRANADE, 80, 350);
				spawnWeapon(WeaponTypes.WEAPON_MG, 80, 350);
				spawnWeapon(WeaponTypes.WEAPON_SPIN, 80, 350);
				spawnWeapon(WeaponTypes.WEAPON_HANDGUN, 80, 350);
				spawnWeapon(WeaponTypes.WEAPON_SHOOTGUN, 80, 350);
			}else {
				spawnWeapon(MiscMath.randomNumber(0, 5), 80, 350);
			}
			
			_spawmWeaponTimer = TweenLite.delayedCall(0.20, letsSpawnWeapons);
		}
		
		//Spawm Weapon Logic
		private function spawnWeapon(type:int, fromDistance:int, toDistance:int):void {
			var weapon:WeaponGameObject;
			var currentWeaponAmount:int;
			var posAng:PositionAngleVo;
			
			if (_player) {
				posAng = randomPosAngleVo(fromDistance, toDistance, fromDistance, toDistance, 0, 359, true);
				
				currentWeaponAmount = objectPool.getActiveGameObjectIdsByType(GameObjectTypes.WEAPON_TYPE).length;
				if (MainConfig.MAP_MAX_WEAPONS >= currentWeaponAmount) {
					weapon = createWeapon(type);
					weapon.rotation = posAng.rotation;
					weapon.x = _player.x + posAng.x;
					weapon.y = _player.y + posAng.y;
				}
			}
		}
		
		//Spawm Creaps Logic
		private function spawnCreaps(type:int, count:int, fromDistance:int, toDistance:int):void {
			var creature:EnemyGameObject;
			var posAng:PositionAngleVo;
			
			var currentEnemyAmount:int = objectPool.getActiveGameObjectIdsByType(GameObjectTypes.ENEMY_TYPE).length;
			if (_player) {
				if (MainConfig.MAP_MAX_ENEMIES >= currentEnemyAmount) {
					for (var i:int = 0; i < count; i++) {
						posAng = randomPosAngleVo(fromDistance, toDistance, fromDistance, toDistance, 0, 359, true);
						creature = createEnemie(type);
						creature.rotation = posAng.rotation;
						creature.x = _player.x + posAng.x;
						creature.y = _player.y + posAng.y;
					}
				}
			}
		}
		
		private function hitTestMainGameObject(gameObject:MainGameObject, gameObject2:MainGameObject):Boolean {
			return (MiscMath.fDistance(gameObject.x, gameObject.y, gameObject2.x, gameObject2.y) <= (gameObject.hitRange + gameObject2.hitRange));
			//return gameObject.getBounds(gameObject.parent).intersects(gameObject2.getBounds(gameObject2.parent));
		}
		
		private function onEnterFrame(e:EnterFrameEvent):void {
			var i:int;
			var ii:int;
			
			
			if (_player) {
				//Update player
				_player.update();
				//Update view
				viewUpdate();
			}
			
			//Get the current Player-Postion
			var playerPosAng:PositionAngleVo = _player.positionAngleVo;
			
			
			//Get the Bullets from the ObjectPool
			var activeBulletIds:Vector.<int> = objectPool.getActiveGameObjectIdsByType(GameObjectTypes.BULLET_TYPE);
			var bulletGameObj:BulletGameObject;
			var activeBulletLenght:int = activeBulletIds.length;
			var bulletId:int;
			
			//Get the Enemies from the ObjectPool
			var activeEnemyIds:Vector.<int> = objectPool.getActiveGameObjectIdsByType(GameObjectTypes.ENEMY_TYPE);
			var enemyGameObj:EnemyGameObject;
			var activeEnemyLenght:int = activeEnemyIds.length;
			var enemyId:int;
			
			//Get the Weapons from the ObjectPool
			var activeWeaponIds:Vector.<int> = objectPool.getActiveGameObjectIdsByType(GameObjectTypes.WEAPON_TYPE);
			var weaponGameObj:WeaponGameObject;
			var activeWeaponLenght:int = activeWeaponIds.length;
			var weaponId:int;
			
			for (i = 0; i < activeWeaponIds.length; i++) {
				weaponId = activeWeaponIds[i];
				weaponGameObj = objectPool.getGameObjectById(weaponId) as WeaponGameObject;
				
				//Have the Weapon not an owner, check if the player is collecting the weapon.
				if (!weaponGameObj.hasOwner) {
					if (hitTestMainGameObject(weaponGameObj, _player)) {
						_player.addWeapon(weaponGameObj, HeroGameObject.WEAPON_SECONDARY);
					}
				}
				
			}
			
			//Update the bullet Logic
			for (i = 0; i < activeBulletLenght; i++) {
				bulletId = activeBulletIds[i];
				bulletGameObj = objectPool.getGameObjectById(bulletId) as BulletGameObject;
				//Update the bullet Logic -> fly
				bulletGameObj.update();
			}
			
			//Update the Enemie Logic
			for (ii = 0; ii < activeEnemyIds.length; ii++) {
				enemyId = activeEnemyIds[ii];
				enemyGameObj = objectPool.getGameObjectById(enemyId) as EnemyGameObject;
				
				// Update the target position from the hero
				enemyGameObj.updateTargetPos(playerPosAng);
				
				// Update Enemie-Logic /Walk
				enemyGameObj.update();
				
				// If the enemy hit the player
				if (hitTestMainGameObject(enemyGameObj, _player)) {
					enemyGameObj.removeMe();
					_player.damage(enemyGameObj.hitPoints);
				}
				
				//Get all Active Bullet-Ids
				activeBulletIds = objectPool.getActiveGameObjectIdsByType(GameObjectTypes.BULLET_TYPE);
				
				for (i = 0; i < activeBulletIds.length; i++) {
					bulletId = activeBulletIds[i];
					bulletGameObj = objectPool.getGameObjectById(bulletId) as BulletGameObject;
					if (hitTestMainGameObject(bulletGameObj, enemyGameObj)) {
						enemyGameObj.damage(bulletGameObj.bulletConfig.damage);
						bulletGameObj.hitEnemy();
					}
				}
			}
		}
		
		//Update the Playfieldview
		private function viewUpdate():void {
			var calcX:Number = (_player.x * -1) + (stage.stageWidth / 2);
			var calcY:Number = (_player.y * -1) + (stage.stageHeight / 2);
			
			var maxX:Number = (MainConfig.MAP_MAX_WIDTH*-1) + (stage.stageWidth);
			var maxY:Number = (MainConfig.MAP_MAX_HEIGHT*-1) + (stage.stageHeight);
			
			if (MiscMath.checkNumberBetween(maxX, calcX, 0)) {
				_playFieldArea.x = calcX;
			}
			
			if (MiscMath.checkNumberBetween(maxY, calcY, 0)) {
				_playFieldArea.y = calcY;
			}
		}
		
		//Create a Enemie
		private function createEnemie(type:int):EnemyGameObject {
			var enemy:EnemyGameObject = _gameObjectManager.createEnemy(type);
			if (!_playFieldArea.contains(enemy.mainGraphic)) addChildToPlayFieldLayer(enemy);
			return enemy;
		}
		
		//Create a Weapon
		private function createWeapon(type:int):WeaponGameObject {
			var weapon:WeaponGameObject = _gameObjectManager.createWeapon(type);
			if (!_playFieldArea.contains(weapon.mainGraphic)) addChildToPlayFieldLayer(weapon);
			return weapon;
		}
		
		//Create a Bullet
		private function createBullet(type:int):BulletGameObject {
			var bullet:BulletGameObject = _gameObjectManager.createBullet(type);
			if (!_playFieldArea.contains(bullet.mainGraphic)) addChildToPlayFieldLayer(bullet);
			return bullet;
		}
		
		
		//Signals form the GameObjects
		private function interactPlayfieldSignal(event:EventGameObject):void {
			
			//On create a GameObject
			if (event.eventType == EventGameObject.CREATE_GAME_OBJECT) {
				var eventC:EventCreateGameObject = (event as EventCreateGameObject);
				if (eventC.objectType == GameObjectTypes.BULLET_TYPE) {
					var bullet:BulletGameObject = createBullet(eventC.subType);
					bullet.setPositionAngle(eventC.createPosAngle);
				}else if (eventC.objectType == GameObjectTypes.ENEMY_TYPE) {
					var enemy:EnemyGameObject = createEnemie(eventC.subType);
					enemy.setPositionAngle(eventC.createPosAngle);
				}
				
			//On remove a GameObject
			}else if (event.eventType == EventGameObject.REMOVE_GAME_OBJECT) {
				var eventRm:EventRemoveGameObject = (event as EventRemoveGameObject);
				
				if (eventRm.targetType == GameObjectTypes.HERO_TYPE) {
					_player.deactivate();
					gameOver();
				}else {
					_gameObjectManager.removeGameObjectById(eventRm.targetId);
				}
				
			//On get a Info a GameObject (the health of the hero)
			}else if (event.eventType == EventGameObject.INFO_GAME_OBJECT) {
				var eventInfo:EventInfoGameObject = (event as EventInfoGameObject);
				if (eventInfo.gameObjectType == GameObjectTypes.HERO_TYPE) {
					_hud.currentHealth = eventInfo.health;
				}
				
			}
		}
		
		//Gamepad Interaction
		public function interactGamepad(gamepad:GamePadState):void {
			if ((_player) && (_player.isAlive)) {
				//_player.turn(mouseState.position);
				
				if (gamepad.lastInteraction == ControlButtonsConst.BUTTON_LT) {
					_player.weaponInteraction(HeroGameObject.WEAPON_PRIMARY, gamepad.buttonLT);
				}else if (gamepad.lastInteraction == ControlButtonsConst.BUTTON_RT) {
					_player.weaponInteraction(HeroGameObject.WEAPON_SECONDARY, gamepad.buttonRT);
				}else if (gamepad.lastInteraction == ControlButtonsConst.BUTTON_LEFT) {
					_player.updateMove(HeroGameObject.MOVE_DIRECTION_LEFT, gamepad.left);
				}else if (gamepad.lastInteraction == ControlButtonsConst.BUTTON_RIGHT) {
					_player.updateMove(HeroGameObject.MOVE_DIRECTION_RIGHT, gamepad.right);
				}else if (gamepad.lastInteraction == ControlButtonsConst.BUTTON_UP) {
					_player.updateMove(HeroGameObject.MOVE_DIRECTION_UP, gamepad.up);
				}else if (gamepad.lastInteraction == ControlButtonsConst.BUTTON_DOWN) {
					_player.updateMove(HeroGameObject.MOVE_DIRECTION_DOWN, gamepad.down);
				}else if (gamepad.lastInteraction == ControlButtonsConst.BUTTON_JOY_X_AXIS) {
					_player.updateMove(HeroGameObject.MOVE_DIRECTION_LEFT, gamepad.left);
					_player.updateMove(HeroGameObject.MOVE_DIRECTION_RIGHT, gamepad.right);
				}else if (gamepad.lastInteraction == ControlButtonsConst.BUTTON_JOY_Y_AXIS) {
					_player.updateMove(HeroGameObject.MOVE_DIRECTION_DOWN, gamepad.down);
					_player.updateMove(HeroGameObject.MOVE_DIRECTION_UP, gamepad.up);
				}else if (gamepad.lastInteraction == ControlButtonsConst.BUTTON_RIGHTSTICK) {
					_player.changeDegrees(gamepad.rotation);
					
				}
			}
		}
		
		//Mouse Interaction
		private function interactMouse(mouseState:MouseState):void {
			if ((_player) && (_player.isAlive)) {
				//_player.turn(mouseState.position);
				
				if (mouseState.lastAction == MouseState.MOUSE_LEFT) {
					_player.weaponInteraction(HeroGameObject.WEAPON_PRIMARY, mouseState.leftButtonPressed);
				}else if (mouseState.lastAction == MouseState.MOUSE_RIGHT) {
					_player.weaponInteraction(HeroGameObject.WEAPON_SECONDARY, mouseState.rightButtonPressed);
				}
				
			}
		}		
		
		//Touch Interaction
		private function interactTouch(touchState:TouchState):void {
			if ((_player) && (_player.isAlive)) {
				touchState.position.x -= _playFieldArea.x;
				touchState.position.y -= _playFieldArea.y;
				_player.turn(touchState.position);
			}
		}
		
		//Keyboard Interaction
		private function interactKeyBoard(keyState:KeyState):void {
			if ((_player) && (_player.isAlive)) {
				if (keyState.keyCode == KeyboardInput.KEY_UP) {
					_player.updateMove(HeroGameObject.MOVE_DIRECTION_UP, keyState.keyPressed);
				}else if (keyState.keyCode == KeyboardInput.KEY_LEFT) {
					_player.updateMove(HeroGameObject.MOVE_DIRECTION_LEFT, keyState.keyPressed);
				}else if (keyState.keyCode == KeyboardInput.KEY_RIGHT) {
					_player.updateMove(HeroGameObject.MOVE_DIRECTION_RIGHT, keyState.keyPressed);
				}else if (keyState.keyCode == KeyboardInput.KEY_DOWN) {
					_player.updateMove(HeroGameObject.MOVE_DIRECTION_DOWN, keyState.keyPressed);
				}
			}
		}
		
		//Get a Random Number for a Spawn-Item
		private function randomPosAngleVo(fromX:int, toX:int, fromY:int, toY:int, fromRand:int = 0, toRand:int = 359, PosNegRand:Boolean= false):PositionAngleVo {
			var result:PositionAngleVo = new PositionAngleVo();
			
			result.x = MiscMath.randomNumber(fromX, toX);
			result.y = MiscMath.randomNumber(fromY, toY);
			result.rotation = MiscMath.randomNumber(fromRand, toRand);
			
			if (PosNegRand) {
				if (MiscMath.randomNumber(0, 100) > 50) {
					result.x = result.x * -1;
				}
				if (MiscMath.randomNumber(0, 100) > 50) {
					result.y = result.y * -1;
				}
			}
			return result;
		}
		
		//Getter && Setter
		public function get objectPool():GameObjectPool {
			return _gameObjectManager.objectPool;
		}		
		
		public function get mainConfig():MainConfig {
			return _gameObjectManager.mainConfig;
		}
		
		public function get graphicLib():GraphicLibrary {
			return _gameObjectManager.graphicLib;
		}
		
		public function get background():DisplayObject {
			return _background;
		}
		
		
		
		
	}

}