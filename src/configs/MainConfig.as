package configs 
{
	import configs.gameObjects.constants.BulletTypes;
	import configs.gameObjects.constants.EnemyTypes;
	import configs.gameObjects.constants.WeaponTypes;
	import configs.gameObjects.logics.bullet.ballistic.BulletAutoTriggerBallisticConfig;
	import configs.gameObjects.logics.bullet.ballistic.BulletNormalBallisticConfig;
	import configs.gameObjects.logics.bullet.ballistic.BulletSpinBallisticConfig;
	import configs.gameObjects.logics.bullet.ballistic.BulletWaveBallisticConfig;
	import configs.gameObjects.logics.bullet.impact.BulletDeflectImpactConfig;
	import configs.gameObjects.logics.bullet.impact.BulletNormalImpactConfig;
	import configs.gameObjects.logics.bullet.impact.BulletPenetrationImpactConfig;
	import configs.gameObjects.logics.weapon.shoot.WeaponBurstShotConfig;
	import configs.gameObjects.logics.weapon.shoot.WeaponSingleShotConfig;
	import configs.gameObjects.logics.weapon.shoot.WeaponTribleShotConfig;
	import configs.gameObjects.module.BulletSetupModule;
	import configs.gameObjects.module.EnemySetupModule;
	import configs.gameObjects.module.WeaponSetupModule;
	import configs.gameObjects.vo.BulletConfigVo;
	import configs.gameObjects.vo.EnemyConfigVo;
	import configs.gameObjects.vo.WeaponConfigVo;
	import configs.gameObjects.logics.enemie.decease.EnemyExplodeDeceaseConfig;
	import configs.gameObjects.logics.enemie.decease.EnemyNormalDeceaseConfig;
	import configs.gameObjects.logics.enemie.decease.MainEnemyDeceaseConfig;
	import configs.gameObjects.logics.enemie.walk.EnemyJumpWalkConfig;
	import configs.gameObjects.logics.enemie.walk.EnemyNormalWalkConfig;
	import configs.gameObjects.logics.enemie.walk.EnemyTeleportWalkConfig;
	import configs.gameObjects.vo.PlayerConfigVo;
	import configs.gameObjects.constants.*;
	import configs.gameObjects.logics.bullet.ballistic.*;
	import configs.gameObjects.logics.bullet.impact.*;
	import configs.gameObjects.logics.weapon.shoot.*;
	import configs.gameObjects.module.*;
	import configs.gameObjects.vo.*;
	import flash.utils.Dictionary;
	import graphics.GraphicLibrary;
	import playfield.gameItems.logicModules.enemie.decease.EnemyExplodeDecease;
	import playfield.gameItems.logicModules.enemie.walk.EnemyJumpWalk;
	import playfield.gameItems.logicModules.weapon.shoot.WeaponBurstShot;
	import playfield.gameItems.logicModules.weapon.shoot.WeaponTribleShot;
	/**
	 * ...
	 * @author Der Korser
	 */
	public class MainConfig {
		
		public static const MAP_MAX_HEIGHT:int = 2000;
		public static const MAP_MAX_WIDTH:int = 2000;
		
		public static const MAP_MAX_ENEMIES:int = 75;
		public static const MAP_MAX_WEAPONS:int = 6;
		
		
		private var _playerConfig:PlayerConfigVo;
		
		private var _weaponModules:Dictionary;
		private var _bulletModules:Dictionary;
		private var _enemieModules:Dictionary;
		
		public function MainConfig() {
			
			
			//Player Setup -> Hero-Config
			_playerConfig = new PlayerConfigVo(300, 3, GraphicLibrary.HERO_GRAPHIC, WeaponTypes.WEAPON_HANDGUN);
			
			 /* 
			 * In the Setup-Module are the Main-Config in this Config are the 
			 * 		
			 * 		MainConfigVo -> 	here are the Main-Setup-Informations of the gameobject.
			 * 
			 * 		some MainLogicConfigs ->	this Logic configs have the Informations of the logic-configs are linked to a logic-class with Interfaces (configs.configHeads.logics.MainLogicConfig)
			 * 									the logic-configs are in the tree -> configs.gameObjects.logics
			 * 									the logic-class are the the tree -> playfield.gameItems.logicModules
			 * 		
			 * 		If a GameObject is creating the logic automaticly and initialized and running the functions they are mapped to the interface.
			 * 		
			 * 		Example look at this Structur look in the Source: 
			 * 		Logic-Config-Class:		configs.gameObjects.logics.weapon.shoot.WeaponSingleShotConfig
			 * 		Logic-Runtime-Class:	playfield.gameItems.logicModules.weapon.shoot.WeaponSingleShot 
			 * 		Logic-Game-Interface:	playfield.gameItems.logicModules.weapon.shoot.IWeaponShoot
			 * 		
			 * 		
			 * 		The Main GameObjects are in the Tree: playfield.gameItems
			 * 		
			 * 		This GameObjects are initialize and executing the Logic-Classes.		
			 * 
			 * 		Pooled Game Objects with Logic-Classes:
			 * 		- BulletGameObject
			 * 		- EnemyGameObject
			 * 		- WeaponGameObject
			 * 		
			 * 		Not Pooled and has no Logic-Classes:
			 * 		- HeroGameObject
			 * 		
			 * */
			
			//ENEMIE SETUP
			_enemieModules = new Dictionary();
			
			/* 
			 * Every EnemySetupModule have a Main 
			 * EnemyConfigVo -> Asset-Id, hitPoints, max-Energy, max-speed, min-speed
			 * 
			 * And the logic-configs:
			 * MainEnemyDeceaseConfig -> what happen when the enemy dies...
			 * MainEnemyWalkConfig -> the walking logic class
			 * */
			
			_enemieModules[EnemyTypes.JUMPER_ENEMIE] = new EnemySetupModule(
			
			// EnemySetupModule has the information of the Asset-Id, hitPoints, max-Energy, max-speed, min-speed
			new EnemyConfigVo(EnemyTypes.JUMPER_ENEMIE, GraphicLibrary.ENEMY2_GRAPHIC, 25, 80, 6, 9),
			
			/*  EnemyExplodeDeceaseConfig
			 *  The Enemy will explode and create new Enemies
			 * 
			 *  _radius:Number -> explosion _radius
			 *  _spawnEnemieType:int -> EnemieTypeId
			 *  _minAmount:int -> min spawn Enemie
			 *  _maxAmount:int -> max spawn Enemie
			 * */
			new EnemyExplodeDeceaseConfig(30, EnemyTypes.MINI_ENEMIE, 5, 15),
			
			/*  EnemyJumpWalkConfig
			 *  The Enemy will explode and create new Enemies
			 * 
			 *  _delayMinTime:Number -> minDelay jump time
			 *  _delayMaxTime:Number -> maxDelay jump time
			 *  
			 *  good to know:
			 *  the jumping distance is the speed of the EnemyConfigVo
			 * */
			new EnemyJumpWalkConfig(30, 60));
			
			_enemieModules[EnemyTypes.CRIC_ENEMIE] = new EnemySetupModule(
				new EnemyConfigVo(EnemyTypes.CRIC_ENEMIE, GraphicLibrary.ENEMY1_GRAPHIC, 15, 50, 3, 1),
				new EnemyNormalDeceaseConfig(),
				new EnemyNormalWalkConfig());
				
			_enemieModules[EnemyTypes.MINI_ENEMIE] = new EnemySetupModule(
				new EnemyConfigVo(EnemyTypes.MINI_ENEMIE, GraphicLibrary.ENEMY3_GRAPHIC, 10, 30, 3.5, 1),
				new EnemyNormalDeceaseConfig(),
				new EnemyNormalWalkConfig());
				
			_enemieModules[EnemyTypes.PORTER_ENEMIE] = new EnemySetupModule(
				new EnemyConfigVo(EnemyTypes.PORTER_ENEMIE, GraphicLibrary.ENEMY4_GRAPHIC, 20, 80, 3, 2),
				new EnemyNormalDeceaseConfig(),
				new EnemyTeleportWalkConfig(100, 150, 200, 300));
			
			
			
			//WEAPON SETUP
			_weaponModules = new Dictionary();
			_weaponModules[WeaponTypes.WEAPON_HANDGUN] = new WeaponSetupModule(
				new WeaponConfigVo(WeaponTypes.WEAPON_HANDGUN, BulletTypes.NORMAL_GUN_BULLET, GraphicLibrary.WEAPON1_GRAPHIC, -1, 0.35),
				new WeaponSingleShotConfig(2));
				
			_weaponModules[WeaponTypes.WEAPON_WAVE] = new WeaponSetupModule(
				new WeaponConfigVo(WeaponTypes.WEAPON_HANDGUN, BulletTypes.WAVE_BULLET, GraphicLibrary.WEAPON2_GRAPHIC, -1, 0.03),
				new WeaponBurstShotConfig(1,3,5,15));
				
			_weaponModules[WeaponTypes.WEAPON_SPIN] = new WeaponSetupModule(
				new WeaponConfigVo(WeaponTypes.WEAPON_SPIN, BulletTypes.SPIN_BULLET, GraphicLibrary.WEAPON3_GRAPHIC, -1, 0.4),
				new WeaponTribleShotConfig(35));
				
			_weaponModules[WeaponTypes.WEAPON_GRANADE] = new WeaponSetupModule(
				new WeaponConfigVo(WeaponTypes.WEAPON_GRANADE, BulletTypes.GRANADE_EXPLOSIVE_BULLET, GraphicLibrary.WEAPON4_GRAPHIC, -1, 0.5), 
				new WeaponSingleShotConfig(1));
			
			_weaponModules[WeaponTypes.WEAPON_MG] = new WeaponSetupModule(
				new WeaponConfigVo(WeaponTypes.WEAPON_MG, BulletTypes.MG_BULLET, GraphicLibrary.WEAPON5_GRAPHIC, -1, 0.04), 
				new WeaponSingleShotConfig(2));
			
			_weaponModules[WeaponTypes.WEAPON_SHOOTGUN] = new WeaponSetupModule(
				new WeaponConfigVo(WeaponTypes.WEAPON_SHOOTGUN, BulletTypes.SHOTGUN_BULLET, GraphicLibrary.WEAPON6_GRAPHIC, -1, 0.75), 
				new WeaponBurstShotConfig(20,25,10,25));
			
			
			//BULLET SETUP
			_bulletModules = new Dictionary();
			_bulletModules[BulletTypes.NORMAL_GUN_BULLET] = new BulletSetupModule(
				new BulletConfigVo(BulletTypes.NORMAL_GUN_BULLET, 3, 50, 12, GraphicLibrary.BULLET1_GRAPHIC), 
				new BulletNormalImpactConfig(),
				new BulletNormalBallisticConfig());
				
			_bulletModules[BulletTypes.MG_BULLET] = new BulletSetupModule(
				new BulletConfigVo(BulletTypes.MG_BULLET, 3, 10, 12, GraphicLibrary.BULLET6_GRAPHIC), 
				new BulletPenetrationImpactConfig(25),
				new BulletNormalBallisticConfig());
				
			_bulletModules[BulletTypes.WAVE_BULLET] = new BulletSetupModule(
				new BulletConfigVo(BulletTypes.WAVE_BULLET, 0.5, 10, 15, GraphicLibrary.BULLET3_GRAPHIC),
				new BulletPenetrationImpactConfig(50),
				new BulletWaveBallisticConfig(0.2, 0.6, 5, 10));
				
			_bulletModules[BulletTypes.SPIN_BULLET] = new BulletSetupModule(
				new BulletConfigVo(BulletTypes.SPIN_BULLET, 6, 4, 6, GraphicLibrary.BULLET4_GRAPHIC),
				new BulletDeflectImpactConfig(15,45), 
				new BulletSpinBallisticConfig(50));
				
			_bulletModules[BulletTypes.GRANADE_EXPLOSIVE_BULLET] = new BulletSetupModule(
				new BulletConfigVo(BulletTypes.GRANADE_EXPLOSIVE_BULLET, 5, 3, 8, GraphicLibrary.BULLET2_GRAPHIC), 
				new BulletExplosionImpactConfig(30, 60, BulletTypes.SHRAPNELL_BULLET),
				new BulletAutoTriggerBallisticConfig(30,40));			
				
			_bulletModules[BulletTypes.SHRAPNELL_BULLET] = new BulletSetupModule(
				new BulletConfigVo(BulletTypes.SHRAPNELL_BULLET, 0.45, 6, 6, GraphicLibrary.BULLET5_GRAPHIC), 
				new BulletDeflectImpactConfig(90,260),
				new BulletSpinBallisticConfig(75));
				
			_bulletModules[BulletTypes.SHOTGUN_BULLET] = new BulletSetupModule(
				new BulletConfigVo(BulletTypes.SHOTGUN_BULLET, 0.6, 20, 16, GraphicLibrary.BULLET7_GRAPHIC), 
				new BulletNormalImpactConfig(),
				new BulletNormalBallisticConfig());
				
		}
		
		public function getWeaponModuleByType(type:int):WeaponSetupModule {
			if (_weaponModules[type]) {
				return _weaponModules[type];
			}
			
			return null;
		}
		
		public function getBulletModulesByType(type:int):BulletSetupModule {
			if (_bulletModules[type]) {
				return _bulletModules[type];
			}
			
			return null;
		}
		
		public function getEnemyModulesByType(type:int):EnemySetupModule {
			if (_enemieModules[type]) {
				return _enemieModules[type];
			}
			
			return null;
		}
		
		public function get playerConfig():PlayerConfigVo {
			return _playerConfig;
		}
		
	}

}