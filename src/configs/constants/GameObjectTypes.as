package configs.constants {
	/**
	 * ...
	 * @author kk
	 */
	public class GameObjectTypes {
		
		public static const WEAPON_TYPE		:int = 0;
		public static const BULLET_TYPE		:int = 1;
		public static const HERO_TYPE		:int = 2;
		public static const ENEMY_TYPE		:int = 3;
		
	}

}