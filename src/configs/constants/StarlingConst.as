package configs.constants 
{
	/**
	 * ...
	 * @author Der Korser
	 */
	public class StarlingConst 
	{
		public static const STARLING_FPS		:int = 60;
		
		public static const STARLING_360_DEG	:Number = 6.2831853072;
		public static const STARLING_180_DEG	:Number = 3.1415926536;
		public static const STARLING_90_DEG		:Number = 1.5707963267948966;
		public static const STARLING_45_DEG		:Number = 0.7853981634;
		public static const STARLING_30_DEG		:Number = 0.5235987756;
		public static const STARLING_10_DEG		:Number = 0.1745329252;
	}

}