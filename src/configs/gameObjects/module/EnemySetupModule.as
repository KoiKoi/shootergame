package configs.gameObjects.module {
	import configs.gameObjects.logics.enemie.decease.MainEnemyDeceaseConfig;
	import configs.gameObjects.logics.enemie.walk.MainEnemyWalkConfig;
	import configs.gameObjects.vo.EnemyConfigVo;
	/**
	 * ...
	 * @author Der Korser
	 */
	
	public class EnemySetupModule {
		
		private var _configVo				:EnemyConfigVo;
		private var _deceaseLogicConfig		:MainEnemyDeceaseConfig;
		private var _walkLogicConfig		:MainEnemyWalkConfig;
		
		public function EnemySetupModule(enemyConfigVo:EnemyConfigVo, enemyDeceaseLogicConfig:MainEnemyDeceaseConfig, enemyWalkLogicConfig:MainEnemyWalkConfig) {
			_configVo = enemyConfigVo;
			_deceaseLogicConfig = enemyDeceaseLogicConfig;
			_walkLogicConfig = enemyWalkLogicConfig;
		}
		
		public function get configVo():EnemyConfigVo {
			return _configVo;
		}
		
		public function get deceaseLogicConfig():MainEnemyDeceaseConfig {
			return _deceaseLogicConfig;
		}
		
		public function get walkLogicConfig():MainEnemyWalkConfig {
			return _walkLogicConfig;
		}
		
		
	}

}