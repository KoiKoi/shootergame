package configs.gameObjects.module  {
	import configs.gameObjects.logics.bullet.ballistic.MainBulletBalisticConfig;
	import configs.gameObjects.logics.bullet.impact.MainBulletImpactConfig;
	import configs.gameObjects.vo.BulletConfigVo;
	import flash.display.BitmapData;
	/**
	 * ...
	 * @author Der Korser
	 */
	public class BulletSetupModule {
		
		private var _configVo				:BulletConfigVo;
		private var _impactLogicConfig		:MainBulletImpactConfig;
		private var _balisticLogicConfig	:MainBulletBalisticConfig;
		
		public function BulletSetupModule(bulletConfigVo:BulletConfigVo, bulletImpactLogicConfig:MainBulletImpactConfig, bulletBalisticLogicConfig:MainBulletBalisticConfig) {
			_configVo = bulletConfigVo;
			_impactLogicConfig = bulletImpactLogicConfig;
			_balisticLogicConfig = bulletBalisticLogicConfig;
		}
		
		public function get configVo():BulletConfigVo {
			return _configVo;
		}
		
		public function get impactLogicConfig():MainBulletImpactConfig {
			return _impactLogicConfig;
		}
		
		public function get balisticLogicConfig():MainBulletBalisticConfig {
			return _balisticLogicConfig;
		}
		
		
	}

}