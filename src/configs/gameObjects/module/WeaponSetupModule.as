package configs.gameObjects.module {
	import configs.gameObjects.logics.weapon.shoot.MainWeaponShotConfig;
	import configs.gameObjects.logics.weapon.shoot.WeaponSingleShotConfig;
	import configs.gameObjects.vo.WeaponConfigVo;
	/**
	 * ...
	 * @author Der Korser
	 */
	public class WeaponSetupModule {
		
		private var _configVo:WeaponConfigVo;
		private var _shotLogicConfig:MainWeaponShotConfig;
		
		public function WeaponSetupModule(weaponConf:WeaponConfigVo, weaponShotLogicConfig:MainWeaponShotConfig) {
			_configVo = weaponConf;
			_shotLogicConfig = weaponShotLogicConfig;
		}
		
		public function get configVo():WeaponConfigVo {
			return _configVo;
		}
		
		public function get weaponShotLogic():MainWeaponShotConfig {
			return _shotLogicConfig;
		}
		
	}

}