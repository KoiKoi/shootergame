package configs.gameObjects.logics.weapon.shoot 
{
	import configs.configHeads.logics.MainLogicConfig;
	import playfield.gameItems.logicModules.weapon.shoot.WeaponBurstShot;
	/**
	 * ...
	 * @author kk
	 */
	public class WeaponBurstShotConfig extends MainWeaponShotConfig {
		
		private var _spreading:Number;
		private var _offsetBetweenShots:Number;
		private var _amountMin:int;
		private var _amountMax:int;
		
		public function WeaponBurstShotConfig(shootAmountMin:int = 3, shootAmountMax:int = 5, offsetBetweenShots:Number = 0, shootSpreading:Number = 0) {
			logicClass = WeaponBurstShot;
			_offsetBetweenShots = offsetBetweenShots;
			_spreading = shootSpreading;
			_amountMin = shootAmountMin;
			_amountMax = shootAmountMax;
		}
		
		public function get offsetBetweenShots():Number {
			return _offsetBetweenShots;
		}
		
		public function get spreading():Number {
			return _spreading;
		}
		
		public function get amountMin():int {
			return _amountMin;
		}
		
		public function get amountMax():int {
			return _amountMax;
		}
		
		
	}

}