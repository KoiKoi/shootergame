package configs.gameObjects.logics.weapon.shoot 
{
	import configs.configHeads.logics.MainLogicConfig;
	import playfield.gameItems.logicModules.weapon.shoot.WeaponSingleShot;
	/**
	 * ...
	 * @author kk
	 */
	public class WeaponSingleShotConfig extends MainWeaponShotConfig  {
		
		private var _accuracy:Number;
		
		public function WeaponSingleShotConfig(shootAccuracy:Number = 0) {
			// creates the logic class
			logicClass = WeaponSingleShot;
			_accuracy = shootAccuracy;
		}
		
		public function get accuracy():Number {
			return _accuracy;
		}
		
	}

}