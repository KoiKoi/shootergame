package configs.gameObjects.logics.weapon.shoot 
{
	import configs.configHeads.logics.MainLogicConfig;
	import playfield.gameItems.logicModules.weapon.shoot.WeaponSingleShot;
	import playfield.gameItems.logicModules.weapon.shoot.WeaponTribleShot;
	/**
	 * ...
	 * @author kk
	 */
	public class WeaponTribleShotConfig extends MainWeaponShotConfig  {
		
		private var _spreading:Number;
		
		public function WeaponTribleShotConfig(shootSpreading:Number = 2.5) {
			logicClass = WeaponTribleShot;
			_spreading = shootSpreading;
		}
		
		public function get spreading():int {
			return _spreading;
		}
		
		
		
	}

}