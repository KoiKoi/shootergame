package configs.gameObjects.logics.enemie.decease {
	import configs.configHeads.logics.MainLogicConfig;
	import playfield.gameItems.logicModules.enemie.decease.EnemyExplodeDecease;
	/**
	 * ...
	 * @author kk
	 */
	public class EnemyExplodeDeceaseConfig extends MainEnemyDeceaseConfig {
		
		private var _radius:Number;
		private var _spawnEnemieType:int;
		private var _minAmount:int;
		private var _maxAmount:int;
		
		function EnemyExplodeDeceaseConfig(explodeRadius:Number, explodeSpawnEnemieType:int, explodeMinAmount:int, explodeMaxAmount:int):void {
			logicClass = EnemyExplodeDecease;
			
			_radius = explodeRadius;
			_spawnEnemieType = explodeSpawnEnemieType;
			_minAmount = explodeMinAmount;
			_maxAmount = explodeMaxAmount;
		}
		
		public function get radius():Number {
			return _radius;
		}
		
		public function get spawnEnemieType():int {
			return _spawnEnemieType;
		}
		
		public function get minAmount():int {
			return _minAmount;
		}
		
		public function get maxAmount():int {
			return _maxAmount;
		}
		
		
	}

}