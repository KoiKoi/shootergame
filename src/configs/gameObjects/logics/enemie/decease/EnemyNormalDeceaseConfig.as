package configs.gameObjects.logics.enemie.decease {
	import configs.configHeads.logics.MainLogicConfig;
	import playfield.gameItems.logicModules.enemie.decease.EnemyNormalDecease;
	/**
	 * ...
	 * @author kk
	 */
	public class EnemyNormalDeceaseConfig extends MainEnemyDeceaseConfig {
		
		function EnemyNormalDeceaseConfig():void {
			logicClass = EnemyNormalDecease;
		}
		
	}

}