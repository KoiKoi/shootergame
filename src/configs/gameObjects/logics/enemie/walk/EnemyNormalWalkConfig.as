package configs.gameObjects.logics.enemie.walk {
	import configs.configHeads.logics.MainLogicConfig;
	import playfield.gameItems.logicModules.enemie.walk.EnemyNormalWalk;
	
	/**
	 * ...
	 * @author kk
	 */
	public class EnemyNormalWalkConfig extends MainEnemyWalkConfig {
		
		function EnemyNormalWalkConfig():void {
			logicClass = EnemyNormalWalk;
		}
		
	}

}