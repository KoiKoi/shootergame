package configs.gameObjects.logics.enemie.walk {
	import configs.configHeads.logics.MainLogicConfig;
	import playfield.gameItems.logicModules.enemie.walk.EnemyJumpWalk;
	import playfield.gameItems.logicModules.enemie.walk.EnemyTeleportWalk;
	/**
	 * ...
	 * @author kk
	 */
	public class EnemyTeleportWalkConfig extends MainEnemyWalkConfig {
		
		private var _delayMinTime:Number;
		private var _delayMaxTime:Number;
		private var _minDistance:int;
		private var _maxDistance:int;
		
		function EnemyTeleportWalkConfig(teleportMinDelayTime:int, teleportMaxDelayTime:int, minDistance:int, maxDistance:int):void {
			logicClass = EnemyTeleportWalk;
			_delayMinTime = teleportMinDelayTime;
			_delayMaxTime = teleportMaxDelayTime;
			_maxDistance = maxDistance;
			_minDistance = minDistance;
		}
		
		public function get delayMaxTime():int {
			return _delayMaxTime;
		}
		
		public function get delayMinTime():int {
			return _delayMinTime;
		}
		
		public function get minDistance():int {
			return _minDistance;
		}
		
		public function get maxDistance():int {
			return _maxDistance;
		}
		
		
	}

}