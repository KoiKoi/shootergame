package configs.gameObjects.logics.enemie.walk {
	import configs.configHeads.logics.MainLogicConfig;
	import playfield.gameItems.logicModules.enemie.walk.EnemyJumpWalk;
	/**
	 * ...
	 * @author kk
	 */
	public class EnemyJumpWalkConfig extends MainEnemyWalkConfig {
		
		private var _delayMinTime:Number;
		private var _delayMaxTime:Number;
		
		function EnemyJumpWalkConfig(jumpMinDelayTime:int, jumpMaxDelayTime:int):void {
			logicClass = EnemyJumpWalk;
			_delayMinTime = jumpMinDelayTime;
			_delayMaxTime = jumpMaxDelayTime;
		}
		
		public function get delayMaxTime():int {
			return _delayMaxTime;
		}
		
		public function get delayMinTime():int {
			return _delayMinTime;
		}
		
	}

}