package configs.gameObjects.logics.bullet.ballistic {
	import configs.configHeads.logics.MainLogicConfig;
	import playfield.gameItems.logicModules.bullet.ballistic.BulletSpinBallistic;
	/**
	 * ...
	 * @author kk
	 */
	public class BulletSpinBallisticConfig extends MainBulletBalisticConfig {
		
		private var _spin:Number;
		
		function BulletSpinBallisticConfig(bulletSpin:Number = 0.5):void {
			logicClass = BulletSpinBallistic;
			_spin = bulletSpin;
		}
		
		public function get spin():Number {
			return _spin;
		}
		

		
	}

}