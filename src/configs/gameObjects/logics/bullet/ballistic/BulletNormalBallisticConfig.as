package configs.gameObjects.logics.bullet.ballistic {
	import configs.configHeads.logics.MainLogicConfig;
	import playfield.gameItems.logicModules.bullet.ballistic.BulletNormalBallistic;
	/**
	 * ...
	 * @author kk
	 */
	public class BulletNormalBallisticConfig extends MainBulletBalisticConfig {
		
		function BulletNormalBallisticConfig():void {
			logicClass = BulletNormalBallistic;
		}
		
	}

}