package configs.gameObjects.logics.bullet.ballistic {
	import configs.configHeads.logics.MainLogicConfig;
	import playfield.gameItems.logicModules.bullet.ballistic.BulletAutoTriggerBallistic;
	import playfield.gameItems.logicModules.bullet.ballistic.BulletNormalBallistic;
	/**
	 * ...
	 * @author kk
	 */
	public class BulletAutoTriggerBallisticConfig extends MainBulletBalisticConfig {
		
		private var _triggerMinTime:Number;
		private var _triggerMaxTime:Number;
		
		function BulletAutoTriggerBallisticConfig(bulletTriggerMinTime:Number, bulletTriggerMaxTime:Number):void {
			logicClass = BulletAutoTriggerBallistic;
			_triggerMinTime = bulletTriggerMinTime;
			_triggerMaxTime = bulletTriggerMaxTime;
		}
		
		public function get triggerMinTime():Number {
			return _triggerMinTime;
		}
		
		public function get triggerMaxTime():Number {
			return _triggerMaxTime;
		}
		
	}

}