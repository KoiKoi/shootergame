package configs.gameObjects.logics.bullet.ballistic {
	import configs.configHeads.logics.MainLogicConfig;
	import playfield.gameItems.logicModules.bullet.ballistic.BulletNormalBallistic;
	import playfield.gameItems.logicModules.bullet.ballistic.BulletWaveBallistic;
	/**
	 * ...
	 * @author kk
	 */
	public class BulletWaveBallisticConfig extends MainBulletBalisticConfig {
		
		private var _spinFrom:Number;
		private var _spinTo:Number;
		
		private var _frequenceFrom:Number;
		private var _frequenceTo:Number;
		
		function BulletWaveBallisticConfig(bulletSpinFrom:Number, bulletSpinTo:Number, bulletFrequenceFrom:Number, bulletFrequenceTo:Number):void {
			logicClass = BulletWaveBallistic;
			_spinFrom = bulletSpinFrom;
			_spinTo = bulletSpinTo;
			_frequenceFrom = bulletFrequenceFrom;
			_frequenceTo = bulletFrequenceTo;
		}
		
		public function get spinFrom():Number {
			return _spinFrom;
		}
		
		public function get spinTo():Number {
			return _spinTo;
		}
		
		public function get frequenceFrom():Number {
			return _frequenceFrom;
		}
		
		public function get frequenceTo():Number {
			return _frequenceTo;
		}
		
	}

}