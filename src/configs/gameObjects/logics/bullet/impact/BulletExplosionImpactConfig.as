package configs.gameObjects.logics.bullet.impact {
	import playfield.gameItems.logicModules.bullet.impact.BulletExplosionImpact;
	/**
	 * ...
	 * @author kk
	 */
	public class BulletExplosionImpactConfig extends MainBulletImpactConfig {
		
		private var _shrapnelMinAmount:int;
		private var _shrapnelMaxAmount:int;
		private var _shrapnelBulletType:int;
		
		public function BulletExplosionImpactConfig(bulletShrapnelMinAmount:int, bulletShrapnelMaxAmount:int, bulletShrapnelBulletType:int) {
			logicClass = BulletExplosionImpact;
			_shrapnelMinAmount = bulletShrapnelMinAmount;
			_shrapnelMaxAmount = bulletShrapnelMaxAmount;
			_shrapnelBulletType = bulletShrapnelBulletType;
		}
		
		public function get shrapnelBulletType():int {
			return _shrapnelBulletType;
		}
		
		public function get shrapnelMinAmount():int {
			return _shrapnelMinAmount;
		}
		
		public function get shrapnelMaxAmount():int {
			return _shrapnelMaxAmount;
		}
		
	}

}