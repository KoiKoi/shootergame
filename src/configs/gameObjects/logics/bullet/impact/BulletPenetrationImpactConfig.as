package configs.gameObjects.logics.bullet.impact {
	import playfield.gameItems.logicModules.bullet.impact.BulletPenetrationImpact;
	/**
	 * ...
	 * @author kk
	 */
	public class BulletPenetrationImpactConfig extends MainBulletImpactConfig {
		
		private var _percentStick:int;
		
		public function BulletPenetrationImpactConfig(bulletPercentStick:int) {
			logicClass = BulletPenetrationImpact;
			_percentStick = bulletPercentStick;
		}
		
		public function get percentStick():int {
			return _percentStick;
		}
		
	}

}