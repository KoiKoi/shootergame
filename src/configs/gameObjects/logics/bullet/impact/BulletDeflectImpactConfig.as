package configs.gameObjects.logics.bullet.impact 
{
	import playfield.gameItems.logicModules.bullet.impact.BulletDeflectImpact;
	/**
	 * ...
	 * @author kk
	 */
	public class BulletDeflectImpactConfig extends MainBulletImpactConfig {
		
		private var _deflectionMinDegrees:Number;
		private var _deflectionMaxDegrees:Number;
		
		public function BulletDeflectImpactConfig(bulletMinDegrees:Number, bulletMaxDegrees:Number) {
			logicClass = BulletDeflectImpact;
			_deflectionMinDegrees = bulletMinDegrees;
			_deflectionMaxDegrees = bulletMaxDegrees;
		}
		
		public function get deflectionMinDegrees():Number {
			return _deflectionMinDegrees;
		}
		
		public function get deflectionMaxDegrees():Number {
			return _deflectionMaxDegrees;
		}
		
	}

}