package configs.gameObjects.logics.bullet.impact {
	import playfield.gameItems.logicModules.bullet.impact.BulletNormalImpact;
	/**
	 * ...
	 * @author kk
	 */
	public class BulletNormalImpactConfig extends MainBulletImpactConfig {
		
		public function BulletNormalImpactConfig() {
			logicClass = BulletNormalImpact;
		}
		
	}

}