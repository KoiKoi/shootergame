package configs.gameObjects.constants {
	import configs.gameObjects.module.BulletSetupModule;
	/**
	 * ...
	 * @author Der Korser
	 */
	public class EnemyTypes {
		public static const CRIC_ENEMIE:int = 1;
		public static const PORTER_ENEMIE:int = 2;
		public static const MINI_ENEMIE:int = 3;
		public static const JUMPER_ENEMIE:int = 4;
	}

}