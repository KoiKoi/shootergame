package configs.gameObjects.constants {
	/**
	 * ...
	 * @author Der Korser
	 */
	public class WeaponTypes {
		public static const WEAPON_HANDGUN:int = 0;
		public static const WEAPON_MG:int = 1;
		public static const WEAPON_SHOOTGUN:int = 2;
		public static const WEAPON_WAVE:int = 3;
		public static const WEAPON_SPIN:int = 4;
		public static const WEAPON_GRANADE:int = 5;
	}
	
}