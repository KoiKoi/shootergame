package configs.gameObjects.constants {
	import configs.gameObjects.module.BulletSetupModule;
	/**
	 * ...
	 * @author Der Korser
	 */
	public class BulletTypes {
		public static const NORMAL_GUN_BULLET:int = 1;
		public static const GRANADE_EXPLOSIVE_BULLET:int = 2;
		public static const SHOTGUN_BULLET:int = 3;
		public static const SPIN_BULLET:int = 4;
		static public const WAVE_BULLET:int = 5;
		static public const SHRAPNELL_BULLET:int = 6;
		public static const MG_BULLET:int = 7;
	}

}