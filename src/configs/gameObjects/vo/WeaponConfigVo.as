package configs.gameObjects.vo {
	/**
	 * ...
	 * @author Der Korser
	 */
	public class WeaponConfigVo {
		
		public var type:int; 
		public var maxAmmo:int;
		public var delay:Number;
		public var graphicId:int;
		public var bulletType:int;
		
		public function WeaponConfigVo(_type:int, _bulletType:int, _graphicId:int, _maxAmmo:int = -1, _delay:Number = 0.5) {
			type = _type;
			graphicId = _graphicId;
			maxAmmo = _maxAmmo;
			delay = _delay;
			bulletType = _bulletType;
		}
		
	}

}