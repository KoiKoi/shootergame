package configs.gameObjects.vo {

	public class PlayerConfigVo {
		
		public var graphicId:int;
		public var maxEnergy:int;
		public var maxSpeed:Number;
		
		public var primaryWeaponType:Number;
		public var secondaryWeaponType:Number;
		
		public function PlayerConfigVo(_maxEnergy:int, _maxSpeed:Number, _graphicId:int, _primaryWeaponType:int = -1, _secondaryWeaponType:int = -1) {
			maxEnergy = _maxEnergy;
			maxSpeed = _maxSpeed;
			graphicId = _graphicId;
			primaryWeaponType = _primaryWeaponType;
			secondaryWeaponType = _secondaryWeaponType;
		}
		
	}

}