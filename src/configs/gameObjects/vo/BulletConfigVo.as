package configs.gameObjects.vo  {
	
	public class BulletConfigVo {
		
		public var type:int;
		public var lifeTime:Number;
		public var damage:int; 
		public var speed:int;
		public var graphicId:int;
		
		public function BulletConfigVo(_type:int, _lifeTime:Number, _damage:int, _speed:int, _graphicId:int) {
			type = _type;
			lifeTime = _lifeTime;
			damage = _damage;
			speed = _speed;
			graphicId = _graphicId;
		}
		
	}

}