package configs.gameObjects.vo 
{
	/**
	 * ...
	 * @author Der Korser
	 */
	public class EnemyConfigVo {
		
		public var type:int; 
		public var maxEnergy:int;
		public var maxSpeed:Number;
		public var minSpeed:Number;
		public var hitPoints:int;
		public var graphicId:int;
		
		public function EnemyConfigVo(_type:int, _graphicId:int, _hitPoints:int = 10, _maxEnergy:int=100, _maxSpeed:Number=5, _minSpeed:Number=2) {
			maxEnergy = _maxEnergy;
			maxSpeed = _maxSpeed;
			minSpeed = _minSpeed;
			graphicId = _graphicId;
			hitPoints = _hitPoints;
			type = _type;
		}
		
	}

}